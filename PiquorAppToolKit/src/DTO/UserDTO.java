package DTO;

public class UserDTO {
	private static String totalPhotographs;
	private static String totalMailSent;
	private static String pendingForUpload;
	private static String pendingForMail;
	private static String totalPrinted;
	
	
	
	public static String getTotalPhotographs() {
		return totalPhotographs;
	}
	public static void setTotalPhotographs(String totalPhotographs) {
		UserDTO.totalPhotographs = totalPhotographs;
	}
	public static String getTotalMailSent() {
		return totalMailSent;
	}
	public static void setTotalMailSent(String totalMailSent) {
		UserDTO.totalMailSent = totalMailSent;
	}
	public static String getPendingForUpload() {
		return pendingForUpload;
	}
	public static void setPendingForUpload(String pendingForUpload) {
		UserDTO.pendingForUpload = pendingForUpload;
	}
	public static String getPendingForMail() {
		return pendingForMail;
	}
	public static void setPendingForMail(String pendingForMail) {
		UserDTO.pendingForMail = pendingForMail;
	}
	public static String getTotalPrinted() {
		return totalPrinted;
	}
	public static void setTotalPrinted(String totalPrinted) {
		UserDTO.totalPrinted = totalPrinted;
	}
	

}
