package DTO;

public class MachineIdDTO {
	
	private static String emailButton;
	private static String facebookButton;
	private static String twitterButton;
	private static String smsButton;
	private static String mmsButton;
	private static String printButton;
	private static String dobScreen;
	private static String askForName;
	private static String askForPhone;
	private static String feedbackScreen;
	
	
	private static String cameraId;
	private static String seeCameraName;
	private static String captureVideo;
	private static String screensaverType;
	private static String photoCounterTiming;
	private static String screensaverTime;
	private static String imageMagickPath;
	private static String cameraResX;
	private static String cameraResY;
	private static String smsPhonePrefix;
	private static String mmsPhonePrefix; 
	private static String machineId;
	
	private static String machineHealthTimePattern="";
	
	private static String mappingCampaignId="";
	private static String mappingCampaignName="";
	
	


	public static String getEmailButton() {
		return emailButton;
	}

	public static void setEmailButton(String emailButton) {
		MachineIdDTO.emailButton = emailButton;
	}

	public static String getFacebookButton() {
		return facebookButton;
	}

	public static void setFacebookButton(String facebookButton) {
		MachineIdDTO.facebookButton = facebookButton;
	}

	public static String getTwitterButton() {
		return twitterButton;
	}

	public static void setTwitterButton(String twitterButton) {
		MachineIdDTO.twitterButton = twitterButton;
	}

	public static String getSmsButton() {
		return smsButton;
	}

	public static void setSmsButton(String smsButton) {
		MachineIdDTO.smsButton = smsButton;
	}

	public static String getMmsButton() {
		return mmsButton;
	}

	public static void setMmsButton(String mmsButton) {
		MachineIdDTO.mmsButton = mmsButton;
	}

	public static String getPrintButton() {
		return printButton;
	}

	public static void setPrintButton(String printButton) {
		MachineIdDTO.printButton = printButton;
	}

	public static String getDobScreen() {
		return dobScreen;
	}

	public static void setDobScreen(String dobScreen) {
		MachineIdDTO.dobScreen = dobScreen;
	}

	public static String getAskForName() {
		return askForName;
	}

	public static void setAskForName(String askForName) {
		MachineIdDTO.askForName = askForName;
	}

	public static String getAskForPhone() {
		return askForPhone;
	}

	public static void setAskForPhone(String askForPhone) {
		MachineIdDTO.askForPhone = askForPhone;
	}

	public static String getCaptureVideo() {
		return captureVideo;
	}

	public static void setCaptureVideo(String captureVideo) {
		MachineIdDTO.captureVideo = captureVideo;
	}

	public static String getScreensaverType() {
		return screensaverType;
	}

	public static void setScreensaverType(String screensaverType) {
		MachineIdDTO.screensaverType = screensaverType;
	}

	public static String getPhotoCounterTiming() {
		return photoCounterTiming;
	}

	public static void setPhotoCounterTiming(String photoCounterTiming) {
		MachineIdDTO.photoCounterTiming = photoCounterTiming;
	}

	public static String getScreensaverTime() {
		return screensaverTime;
	}

	public static void setScreensaverTime(String screensaverTime) {
		MachineIdDTO.screensaverTime = screensaverTime;
	}

	public static String getImageMagickPath() {
		return imageMagickPath;
	}

	public static void setImageMagickPath(String imageMagickPath) {
		MachineIdDTO.imageMagickPath = imageMagickPath;
	}

	public static String getMachineHealthTimePattern() {
		return machineHealthTimePattern;
	}

	public static void setMachineHealthTimePattern(
			String machineHealthTimePattern) {
		MachineIdDTO.machineHealthTimePattern = machineHealthTimePattern;
	}

	public static String getCameraResX() {
		return cameraResX;
	}

	public static void setCameraResX(String cameraResX) {
		MachineIdDTO.cameraResX = cameraResX;
	}

	public static String getCameraResY() {
		return cameraResY;
	}

	public static void setCameraResY(String cameraResY) {
		MachineIdDTO.cameraResY = cameraResY;
	}

	public static String getSeeCameraName() {
		return seeCameraName;
	}

	public static void setSeeCameraName(String seeCameraName) {
		MachineIdDTO.seeCameraName = seeCameraName;
	}

	public static String getCameraId() {
		return cameraId;
	}

	public static void setCameraId(String cameraId) {
		MachineIdDTO.cameraId = cameraId;
	}

	public static String getSmsPhonePrefix() {
		return smsPhonePrefix;
	}

	public static void setSmsPhonePrefix(String smsPhonePrefix) {
		MachineIdDTO.smsPhonePrefix = smsPhonePrefix;
	}

	public static String getMmsPhonePrefix() {
		return mmsPhonePrefix;
	}

	public static void setMmsPhonePrefix(String mmsPhonePrefix) {
		MachineIdDTO.mmsPhonePrefix = mmsPhonePrefix;
	}

	public static String getFeedbackScreen() {
		return feedbackScreen;
	}

	public static void setFeedbackScreen(String feedbackScreen) {
		MachineIdDTO.feedbackScreen = feedbackScreen;
	}

	public static String getMachineId() {
		return machineId;
	}

	public static void setMachineId(String machineId) {
		MachineIdDTO.machineId = machineId;
	}

	public static String getMappingCampaignId() {
		return mappingCampaignId;
	}

	public static void setMappingCampaignId(String mappingCampaignId) {
		MachineIdDTO.mappingCampaignId = mappingCampaignId;
	}

	public static String getMappingCampaignName() {
		return mappingCampaignName;
	}

	public static void setMappingCampaignName(String mappingCampaignName) {
		MachineIdDTO.mappingCampaignName = mappingCampaignName;
	}

	
	

	
	

}
