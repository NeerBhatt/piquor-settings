package Launcher;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JPasswordField;
import javax.swing.JButton;

import GUI.MainMenu;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Launcher extends JFrame {

	private JPanel contentPane;
	private JPasswordField passwordField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Launcher frame = new Launcher();
					frame.setVisible(true);
					frame.setLocationRelativeTo ( null );
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Launcher() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		setBounds(100, 100, 358, 196);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 255, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setFont(new Font("Consolas", Font.PLAIN, 17));
		lblPassword.setBackground(new Color(255, 255, 255));
		lblPassword.setBounds(38, 60, 98, 21);
		contentPane.add(lblPassword);
		
		passwordField = new JPasswordField();
		passwordField.setBackground(new Color(255, 255, 255));
		passwordField.setBounds(128, 60, 177, 21);
		contentPane.add(passwordField);
		
		JLabel lblConfirmYourIdentity = new JLabel("Confirm Your Identity");
		lblConfirmYourIdentity.setFont(new Font("Consolas", Font.PLAIN, 16));
		lblConfirmYourIdentity.setBounds(77, 11, 204, 21);
		contentPane.add(lblConfirmYourIdentity);
		
		JButton btnGo = new JButton("Submit");
		btnGo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(passwordField.getText().equals("Qeadc@123"))
				{
					setVisible(false);
					GUI.MainMenu menu=new MainMenu();
					menu.setVisible(true);
				}
				else
				{
					passwordField.setText("");
					JOptionPane.showMessageDialog(null, "Incorrect Password!");
				}	
			}
		});
		btnGo.setBackground(new Color(255, 255, 255));
		btnGo.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnGo.setBounds(128, 92, 89, 23);
		contentPane.add(btnGo);
	}
}
