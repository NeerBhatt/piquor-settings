package Launcher;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.border.TitledBorder;
import javax.swing.border.LineBorder;
import javax.swing.JCheckBox;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.File;

import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

import component.FeaturePanel;

import DTO.UserDTO;
import Utility.DBUtil;

public class TabbedLauncher extends JFrame {

	private JPanel contentPane;
	private JTextField txtphotocounter;
	private JTextField txtResX;
	private JTextField TxtResY;
	private JTextField txtCameraID;
	private JTextField txtMachineId;
	private JTextField txtCampaignID;
	private JTextField txtCampaignName;
	private JTextField txtSms;
	private JTextField txtMms;
	private JTextField txtScreenSaverTime;
	
	String imagepath="";
	static	String frameId[]=null;
	private JTextField txtThumbnail;
	private JTextField txtForeground;
	private JTextField txtBackground;
	private JTextField txtFrameId;
	private JTextField txtCampaignId;
	private JTextField txtFrameWidth;
	private JTextField txtFrameHeight;
	private JTextField txtRotate;
	private JTextField txtPhotoX;
	private JTextField txtPhotoY;
	private JTextField txtScaleX;
	private JTextField txtScaleY;
	private JTextField textThumbnail;
	private JTextField textForeground;
	private JTextField textBackground;
	private JTextField textFrameWidth;
	private JTextField textRotate;
	private JTextField textFrameHeight;
	private JTextField textPhotoX;
	private JTextField textPhotoY;
	private JTextField textFrameId;
	private JTextField textCampaignId;
	private JTextField textScaleX;
	private JTextField textScaleY;
	JComboBox comboBox = new JComboBox();

	/**
	 * Launch the application.
	 * @throws UnsupportedLookAndFeelException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 * @throws ClassNotFoundException 
	 */
	public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TabbedLauncher frame = new TabbedLauncher();
					frame.setVisible(true);
					frame.setLocationRelativeTo ( null );
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws SQLException 
	 */
	public TabbedLauncher() throws SQLException {
		DAO.MachineConfigFileDAO.loadMachineConfig();
		DAO.MachineConfigFileDAO.loadMachineMapping();
		DAO.UserDAO.userTableReader();
		
		
		setResizable(false);
		setTitle("Settings");
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\Neeraj\\Documents\\Received Files\\camera.png"));
		setBackground(Color.WHITE);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 791, 453);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(248, 248, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.LEFT);
		tabbedPane.setBackground(new Color(192, 192, 192));
		tabbedPane.setBorder(null);
		contentPane.add(tabbedPane);
		
		//JPanel panel_Feature = new JPanel();
		
		FeaturePanel panel_Feature=new FeaturePanel();
		
		panel_Feature.setBorder(new LineBorder(new Color(46, 139, 87), 1, true));
//		panel_Feature.setBackground(new Color(255, 255, 255));
		tabbedPane.addTab("Features", null, panel_Feature, "Select features for app");
		tabbedPane.setBackgroundAt(0, Color.WHITE);
		panel_Feature.setLayout(null);
		
		final JCheckBox chkemail = new JCheckBox("Email");
		chkemail.setRequestFocusEnabled(false);
		chkemail.setFont(new Font("Consolas", Font.PLAIN, 14));
		chkemail.setBackground(Color.WHITE);
		chkemail.setAlignmentX(0.5f);
		chkemail.setBounds(6, 64, 65, 40);
		panel_Feature.add(chkemail);
		
		final JCheckBox chckbxPrint = new JCheckBox("Print");
		chckbxPrint.setRequestFocusEnabled(false);
		chckbxPrint.setFont(new Font("Consolas", Font.PLAIN, 14));
		chckbxPrint.setBackground(Color.WHITE);
		chckbxPrint.setAlignmentX(0.5f);
		chckbxPrint.setBounds(443, 64, 70, 40);
		panel_Feature.add(chckbxPrint);
		
		final JCheckBox chckbxAskForName = new JCheckBox("Ask For Name");
		chckbxAskForName.setRequestFocusEnabled(false);
		chckbxAskForName.setPreferredSize(new Dimension(107, 23));
		chckbxAskForName.setMaximumSize(new Dimension(107, 23));
		chckbxAskForName.setFont(new Font("Consolas", Font.PLAIN, 14));
		chckbxAskForName.setBackground(Color.WHITE);
		chckbxAskForName.setBounds(224, 191, 121, 40);
		panel_Feature.add(chckbxAskForName);
		
		final JCheckBox chckbxDobScreen = new JCheckBox("DOB Screen");
		chckbxDobScreen.setRequestFocusEnabled(false);
		chckbxDobScreen.setFont(new Font("Consolas", Font.PLAIN, 14));
		chckbxDobScreen.setBackground(Color.WHITE);
		chckbxDobScreen.setAlignmentX(0.5f);
		chckbxDobScreen.setBounds(443, 127, 116, 40);
		panel_Feature.add(chckbxDobScreen);
		
		final JCheckBox chckbxRecordvideo = new JCheckBox("Record Video");
		chckbxRecordvideo.setRequestFocusEnabled(false);
		chckbxRecordvideo.setFont(new Font("Consolas", Font.PLAIN, 14));
		chckbxRecordvideo.setBackground(Color.WHITE);
		chckbxRecordvideo.setAlignmentX(0.5f);
		chckbxRecordvideo.setBounds(297, 127, 121, 40);
		panel_Feature.add(chckbxRecordvideo);
		
		final JCheckBox chckbxFeedbackScreen = new JCheckBox("Feedback Screen");
		chckbxFeedbackScreen.setRequestFocusEnabled(false);
		chckbxFeedbackScreen.setForeground(Color.BLACK);
		chckbxFeedbackScreen.setFont(new Font("Consolas", Font.PLAIN, 14));
		chckbxFeedbackScreen.setBackground(Color.WHITE);
		chckbxFeedbackScreen.setBounds(387, 191, 145, 40);
		panel_Feature.add(chckbxFeedbackScreen);
		
		final JCheckBox chckbxAskForPhone = new JCheckBox("Ask For Mobile Number");
		chckbxAskForPhone.setRequestFocusEnabled(false);
		chckbxAskForPhone.setFont(new Font("Consolas", Font.PLAIN, 14));
		chckbxAskForPhone.setBackground(Color.WHITE);
		chckbxAskForPhone.setAlignmentX(0.5f);
		chckbxAskForPhone.setBounds(6, 191, 204, 40);
		panel_Feature.add(chckbxAskForPhone);
		
		final JCheckBox chckbxMms = new JCheckBox("MMS");
		chckbxMms.setRequestFocusEnabled(false);
		chckbxMms.setFont(new Font("Consolas", Font.PLAIN, 14));
		chckbxMms.setBackground(Color.WHITE);
		chckbxMms.setAlignmentX(0.5f);
		chckbxMms.setBounds(133, 127, 77, 40);
		panel_Feature.add(chckbxMms);
		
		final JCheckBox chkFacebook = new JCheckBox("Facebook");
		chkFacebook.setFont(new Font("Consolas", Font.PLAIN, 14));
		chkFacebook.setBackground(Color.WHITE);
		chkFacebook.setAlignmentX(0.5f);
		chkFacebook.setBounds(133, 64, 89, 40);
		panel_Feature.add(chkFacebook);
		
		final JCheckBox chkTwitter = new JCheckBox("Twitter");
		chkTwitter.setRequestFocusEnabled(false);
		chkTwitter.setFont(new Font("Consolas", Font.PLAIN, 14));
		chkTwitter.setBackground(Color.WHITE);
		chkTwitter.setAlignmentX(0.5f);
		chkTwitter.setBounds(297, 64, 89, 40);
		panel_Feature.add(chkTwitter);
		
		final JCheckBox chckbxSms = new JCheckBox("SMS");
		chckbxSms.setRequestFocusEnabled(false);
		chckbxSms.setFont(new Font("Consolas", Font.PLAIN, 14));
		chckbxSms.setBackground(Color.WHITE);
		chckbxSms.setAlignmentX(0.5f);
		chckbxSms.setBounds(6, 127, 70, 40);
		panel_Feature.add(chckbxSms);
		
		JButton button = new JButton("Update");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String email="",facebook="",twitter="",print="",sms="",mms="",askForName="",askForPhone="",feedback="",dob="",recordVideo="";
				if(chkemail.isSelected()==true)
					email="1";
				else
					email="0";
				
				if(chkFacebook.isSelected()==true)
					facebook="1";
				else
					facebook="0";
				
				if(chkTwitter.isSelected()==true)
					twitter="1";
				else
					twitter="0";
				
				if(chckbxAskForName.isSelected()==true)
					askForName="1";
				else
					askForName="0";
				
				if(chckbxAskForPhone.isSelected()==true)
					askForPhone="1";
				else
					askForPhone="0";
				
				if(chckbxDobScreen.isSelected()==true)
					dob="1";
				else
					dob="0";
				
				if(chckbxFeedbackScreen.isSelected()==true)
					feedback="1";
				else
					feedback="0";
				
				if(chckbxMms.isSelected()==true)
					mms="1";
				else
					mms="0";
				
				if(chckbxPrint.isSelected()==true)
					print="1";
				else
					print="0";
				
				if(chckbxSms.isSelected()==true)
					sms="1";
				else
					sms="0";
				
				if(chckbxRecordvideo.isSelected()==true)
					recordVideo="1";
				else
					recordVideo="0";
				try{
					
					DBO.UpdateSettings.updatePiquorAppFeatures(email, facebook, twitter, print, sms, mms, askForName, askForPhone, feedback, dob,recordVideo);
				}
				catch(Exception e)
				{
					
				}
				
				
			}
		});
		button.setFont(new Font("Tahoma", Font.PLAIN, 14));
		button.setBackground(Color.WHITE);
		button.setBounds(240, 340, 89, 23);
		panel_Feature.add(button);
		
		if(DTO.MachineIdDTO.getEmailButton().equals("1"))
		{
			chkemail.setSelected(true);
		}
		if(DTO.MachineIdDTO.getFacebookButton().equals("1"))
		{
			chkFacebook.setSelected(true);
		}
		if(DTO.MachineIdDTO.getTwitterButton().equals("1"))
		{
			chkTwitter.setSelected(true);
		}
		if(DTO.MachineIdDTO.getPrintButton().equals("1"))
		{
			chckbxPrint.setSelected(true);
		}
		if(DTO.MachineIdDTO.getSmsButton().equals("1"))
		{
			chckbxSms.setSelected(true);
		}
		if(DTO.MachineIdDTO.getMmsButton().equals("1"))
		{
			chckbxMms.setSelected(true);
		}
		if(DTO.MachineIdDTO.getDobScreen().equals("1"))
		{
			chckbxDobScreen.setSelected(true);;
		}
		if(DTO.MachineIdDTO.getAskForName().equals("1"))
		{
			chckbxAskForName.setSelected(true);
		}
		if(DTO.MachineIdDTO.getAskForPhone().equals("1"))
		{
			chckbxAskForPhone.setSelected(true);
		}
		if(DTO.MachineIdDTO.getFeedbackScreen().equals("1"))
		{
			chckbxFeedbackScreen.setSelected(true);
		}
		if(DTO.MachineIdDTO.getCaptureVideo().equals("1"))
		{
			chckbxRecordvideo.setSelected(true);
		}
		
		
		JPanel Panel_Campaign_Camera = new JPanel();
		Panel_Campaign_Camera.setBackground(new Color(255, 255, 255));
		tabbedPane.addTab("Campaign and Camera", null, Panel_Campaign_Camera, null);
		Panel_Campaign_Camera.setLayout(null);
		
		JLabel label = new JLabel("Camera Id");
		label.setFont(new Font("Consolas", Font.PLAIN, 14));
		label.setBounds(12, 231, 82, 26);
		Panel_Campaign_Camera.add(label);
		
		JLabel label_1 = new JLabel("Photo Counter Timing");
		label_1.setFont(new Font("Consolas", Font.PLAIN, 14));
		label_1.setBounds(301, 231, 174, 26);
		Panel_Campaign_Camera.add(label_1);
		
		txtphotocounter = new JTextField();
		txtphotocounter.setText((String) null);
		txtphotocounter.setColumns(10);
		txtphotocounter.setBounds(490, 234, 54, 20);
		Panel_Campaign_Camera.add(txtphotocounter);
		
		JLabel label_2 = new JLabel("Camera Resolution X");
		label_2.setFont(new Font("Consolas", Font.PLAIN, 14));
		label_2.setBounds(12, 272, 174, 26);
		Panel_Campaign_Camera.add(label_2);
		
		txtResX = new JTextField();
		txtResX.setText((String) null);
		txtResX.setColumns(10);
		txtResX.setBounds(196, 275, 54, 20);
		Panel_Campaign_Camera.add(txtResX);
		
		JLabel label_3 = new JLabel("Camera Resolution Y");
		label_3.setFont(new Font("Consolas", Font.PLAIN, 14));
		label_3.setBounds(301, 272, 174, 26);
		Panel_Campaign_Camera.add(label_3);
		
		TxtResY = new JTextField();
		TxtResY.setText((String) null);
		TxtResY.setColumns(10);
		TxtResY.setBounds(490, 275, 54, 20);
		Panel_Campaign_Camera.add(TxtResY);
		
		final JCheckBox chckbxSeeCameraName = new JCheckBox("See Camera Name");
		chckbxSeeCameraName.setFont(new Font("Consolas", Font.PLAIN, 14));
		chckbxSeeCameraName.setBackground(Color.WHITE);
		chckbxSeeCameraName.setBounds(12, 189, 148, 35);
		Panel_Campaign_Camera.add(chckbxSeeCameraName);
		
		txtCameraID = new JTextField();
		txtCameraID.setText((String) null);
		txtCameraID.setColumns(10);
		txtCameraID.setBounds(196, 234, 54, 20);
		Panel_Campaign_Camera.add(txtCameraID);
		
		JLabel label_4 = new JLabel("Machine Id");
		label_4.setFont(new Font("Consolas", Font.PLAIN, 14));
		label_4.setBounds(12, 72, 95, 26);
		Panel_Campaign_Camera.add(label_4);
		
		txtMachineId = new JTextField();
		txtMachineId.setText((String) null);
		txtMachineId.setColumns(10);
		txtMachineId.setBounds(120, 75, 106, 20);
		Panel_Campaign_Camera.add(txtMachineId);
		
		JLabel label_5 = new JLabel("Campaign Id");
		label_5.setFont(new Font("Consolas", Font.PLAIN, 14));
		label_5.setBounds(12, 117, 95, 26);
		Panel_Campaign_Camera.add(label_5);
		
		txtCampaignID = new JTextField();
		txtCampaignID.setText((String) null);
		txtCampaignID.setColumns(10);
		txtCampaignID.setBounds(118, 120, 283, 20);
		Panel_Campaign_Camera.add(txtCampaignID);
		
		JLabel label_6 = new JLabel("Campaign Name");
		label_6.setFont(new Font("Consolas", Font.PLAIN, 14));
		label_6.setBounds(273, 72, 107, 26);
		Panel_Campaign_Camera.add(label_6);
		
		txtCampaignName = new JTextField();
		txtCampaignName.setText((String) null);
		txtCampaignName.setColumns(10);
		txtCampaignName.setBounds(396, 75, 148, 20);
		Panel_Campaign_Camera.add(txtCampaignName);
		
		JButton button_1 = new JButton("Update");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String seeCameraName="",CameraId="",PhotoCounterTiming="",CameraResX="",CameraResY="",machineId="",campaignName="",campaignId="";
				if(chckbxSeeCameraName.isSelected()==true)
					seeCameraName="1";
				else
					seeCameraName="0";
				CameraId=txtCameraID.getText();
				PhotoCounterTiming=txtphotocounter.getText();
				CameraResX=txtResX.getText();
				CameraResY=TxtResY.getText();
				machineId=txtMachineId.getText();
				campaignId=txtCampaignID.getText();
				campaignName=txtCampaignName.getText();
				try {
					if(txtMachineId.getText().length()>0 && txtCameraID.getText().length()>0 && txtphotocounter.getText().length()>0 && txtResX.getText().length()>0 && TxtResY.getText().length()>0 && txtCampaignID.getText().length()>0 && txtCampaignName.getText().length()>0)
					{
						DBO.UpdateSettings.updateMachineCore(machineId);
						DBO.UpdateSettings.updateMachineMapping(machineId, campaignId, campaignName);
					DBO.UpdateSettings.updateCameraSettings(machineId,seeCameraName, CameraId,PhotoCounterTiming, CameraResX, CameraResY);
					
					}
					else
						JOptionPane.showMessageDialog(null, "Field(s) are empty!");
					} catch (SQLException ee) {
					// TODO Auto-generated catch block
					ee.printStackTrace();
				}
			}
		});
		button_1.setForeground(Color.BLACK);
		button_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		button_1.setBackground(Color.WHITE);
		button_1.setBounds(242, 342, 89, 26);
		Panel_Campaign_Camera.add(button_1);
		
		
		txtCameraID.setText(DTO.MachineIdDTO.getCameraId());
		txtphotocounter.setText(DTO.MachineIdDTO.getPhotoCounterTiming());
		txtResX.setText(DTO.MachineIdDTO.getCameraResX());
		TxtResY.setText(DTO.MachineIdDTO.getCameraResY());
		txtMachineId.setText(DTO.MachineIdDTO.getMachineId());
		txtCampaignID.setText(DTO.MachineIdDTO.getMappingCampaignId());
		txtCampaignName.setText(DTO.MachineIdDTO.getMappingCampaignName());
		
		
		
		
		JPanel panel_Other = new JPanel();
		panel_Other.setBackground(new Color(255, 255, 255));
		tabbedPane.addTab("Sms,Mms, Screensaver, ImageMagick", null, panel_Other, null);
		panel_Other.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBackground(Color.WHITE);
		panel.setBounds(10, 72, 253, 130);
		panel_Other.add(panel);
		
		JLabel label_7 = new JLabel("SMS Phone Prefix");
		label_7.setFont(new Font("Consolas", Font.PLAIN, 14));
		label_7.setBackground(Color.WHITE);
		label_7.setBounds(10, 52, 131, 21);
		panel.add(label_7);
		
		JLabel label_8 = new JLabel("SMS/MMS");
		label_8.setForeground(new Color(0, 51, 102));
		label_8.setFont(new Font("Consolas", Font.BOLD, 17));
		label_8.setBounds(92, 11, 63, 21);
		panel.add(label_8);
		
		JLabel label_9 = new JLabel("MMS Phone Prefix");
		label_9.setFont(new Font("Consolas", Font.PLAIN, 14));
		label_9.setBackground(Color.WHITE);
		label_9.setBounds(10, 98, 131, 21);
		panel.add(label_9);
		
		txtSms = new JTextField();
		txtSms.setText((String) null);
		txtSms.setColumns(10);
		txtSms.setBounds(151, 52, 86, 20);
		panel.add(txtSms);
		
		txtMms = new JTextField();
		txtMms.setText((String) null);
		txtMms.setColumns(10);
		txtMms.setBounds(151, 98, 86, 20);
		panel.add(txtMms);
		
		JPanel panel_1 = new JPanel();
		panel_1.setLayout(null);
		panel_1.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.setBackground(Color.WHITE);
		panel_1.setBounds(285, 72, 279, 130);
		panel_Other.add(panel_1);
		
		JLabel label_10 = new JLabel("Screensaver Type");
		label_10.setFont(new Font("Consolas", Font.PLAIN, 14));
		label_10.setBackground(Color.WHITE);
		label_10.setBounds(10, 43, 128, 17);
		panel_1.add(label_10);
		
		JLabel label_11 = new JLabel("Screensaver");
		label_11.setForeground(new Color(0, 51, 102));
		label_11.setFont(new Font("Consolas", Font.BOLD, 17));
		label_11.setBounds(93, 11, 99, 21);
		panel_1.add(label_11);
		
		JLabel label_12 = new JLabel("Screensaver Time");
		label_12.setFont(new Font("Consolas", Font.PLAIN, 14));
		label_12.setBackground(Color.WHITE);
		label_12.setBounds(10, 85, 128, 17);
		panel_1.add(label_12);
		
		txtScreenSaverTime = new JTextField();
		txtScreenSaverTime.setText((String) null);
		txtScreenSaverTime.setColumns(10);
		txtScreenSaverTime.setBounds(155, 83, 86, 20);
		panel_1.add(txtScreenSaverTime);
		
		final JRadioButton rdbtnImage = new JRadioButton("Image");
		rdbtnImage.setBackground(Color.WHITE);
		rdbtnImage.setBounds(144, 40, 61, 23);
		panel_1.add(rdbtnImage);
		
		final JRadioButton rdbtnVideo = new JRadioButton("Video");
		rdbtnVideo.setBackground(Color.WHITE);
		rdbtnVideo.setBounds(216, 40, 55, 23);
		panel_1.add(rdbtnVideo);
		
		JPanel panel_6 = new JPanel();
		panel_6.setLayout(null);
		panel_6.setFont(new Font("Tahoma", Font.PLAIN, 11));
		panel_6.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_6.setBackground(Color.WHITE);
		panel_6.setBounds(10, 238, 253, 138);
		panel_Other.add(panel_6);
		
		final JLabel lblpath = new JLabel((String) null);
		lblpath.setFont(new Font("Consolas", Font.PLAIN, 9));
		lblpath.setBackground(Color.WHITE);
		lblpath.setBounds(10, 56, 233, 17);
		panel_6.add(lblpath);
		
		JLabel label_14 = new JLabel("Image Magick Path");
		label_14.setForeground(new Color(0, 51, 102));
		label_14.setFont(new Font("Consolas", Font.BOLD, 17));
		label_14.setBounds(59, 11, 153, 21);
		panel_6.add(label_14);
		
		JButton btnSelectFile = new JButton("Browse Image Magick Path");
		btnSelectFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				 JFileChooser fileChooser = new JFileChooser();
				 fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			        int returnValue = fileChooser.showOpenDialog(null);
			        if (returnValue == JFileChooser.APPROVE_OPTION) {
			          File selectedFile = fileChooser.getSelectedFile();
			          imagepath=selectedFile.getAbsolutePath().replace("\\", "/")+"/convert";
			    
			       lblpath.setText(imagepath);
			       
			        }
			}
		});
		btnSelectFile.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnSelectFile.setBackground(Color.WHITE);
		btnSelectFile.setBounds(20, 84, 222, 23);
		panel_6.add(btnSelectFile);
		
		JButton button_3 = new JButton("Update");
		button_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String smsPhonePrefix="",mmsPhonePrefix="",screensaverType="",ScreenSaverTime="",imageMagickPath="";
				smsPhonePrefix=txtSms.getText();
				mmsPhonePrefix=txtMms.getText();
				ScreenSaverTime=txtScreenSaverTime.getText();
				imageMagickPath=lblpath.getText();
				
				if(rdbtnImage.isSelected()==true)
					screensaverType="image";
				if(rdbtnVideo.isSelected()==true)
					screensaverType="video";
				
				try {
					if(!smsPhonePrefix.equals("") && !mmsPhonePrefix.equals("") && !screensaverType.equals("") && !ScreenSaverTime.equals("") && !imageMagickPath.equals("") )
						DBO.UpdateSettings.updateOtherSettings(smsPhonePrefix, mmsPhonePrefix, screensaverType, ScreenSaverTime, imageMagickPath);
					
					else
						JOptionPane.showMessageDialog(null, "Field(s) Empty!");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		button_3.setFont(new Font("Tahoma", Font.PLAIN, 15));
		button_3.setBackground(Color.WHITE);
		button_3.setBounds(375, 301, 89, 23);
		panel_Other.add(button_3);
		
		if(DTO.MachineIdDTO.getScreensaverType().equals("image"))
			rdbtnImage.setSelected(true);
		else
			rdbtnVideo.setSelected(true);
		
		txtMms.setText(DTO.MachineIdDTO.getMmsPhonePrefix());
		txtSms.setText(DTO.MachineIdDTO.getSmsPhonePrefix());
		txtScreenSaverTime.setText(DTO.MachineIdDTO.getScreensaverTime());
		lblpath.setText(DTO.MachineIdDTO.getImageMagickPath());
		
		
		
		
		JPanel panel_addNewFrame = new JPanel();
		panel_addNewFrame.setBackground(new Color(255, 255, 255));
		tabbedPane.addTab("Add new frame", null, panel_addNewFrame, "Configure new frame");
		panel_addNewFrame.setLayout(null);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_2.setBackground(Color.WHITE);
		panel_2.setLayout(null);
		panel_2.setBounds(10, 72, 272, 113);
		panel_addNewFrame.add(panel_2);
		
		JLabel label_13 = new JLabel("Thumbnail");
		label_13.setFont(new Font("Consolas", Font.PLAIN, 14));
		label_13.setBounds(10, 12, 72, 19);
		panel_2.add(label_13);
		
		txtThumbnail = new JTextField();
		txtThumbnail.setText((String) null);
		txtThumbnail.setColumns(10);
		txtThumbnail.setBounds(156, 11, 102, 20);
		panel_2.add(txtThumbnail);
		
		JLabel label_15 = new JLabel("Foreground Frame");
		label_15.setFont(new Font("Consolas", Font.PLAIN, 14));
		label_15.setBounds(10, 42, 136, 26);
		panel_2.add(label_15);
		
		txtForeground = new JTextField();
		txtForeground.setText((String) null);
		txtForeground.setColumns(10);
		txtForeground.setBounds(156, 42, 102, 20);
		panel_2.add(txtForeground);
		
		JLabel label_16 = new JLabel("Background Frame");
		label_16.setFont(new Font("Consolas", Font.PLAIN, 14));
		label_16.setBounds(10, 70, 138, 26);
		panel_2.add(label_16);
		
		txtBackground = new JTextField();
		txtBackground.setText((String) null);
		txtBackground.setColumns(10);
		txtBackground.setBounds(156, 73, 102, 20);
		panel_2.add(txtBackground);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBackground(Color.WHITE);
		panel_3.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_3.setLayout(null);
		panel_3.setBounds(10, 196, 272, 69);
		panel_addNewFrame.add(panel_3);
		
		JLabel label_17 = new JLabel("Frame Id");
		label_17.setFont(new Font("Consolas", Font.PLAIN, 14));
		label_17.setBounds(14, 11, 100, 26);
		panel_3.add(label_17);
		
		JLabel label_18 = new JLabel("Campaign Id");
		label_18.setFont(new Font("Consolas", Font.PLAIN, 14));
		label_18.setBounds(14, 35, 100, 26);
		panel_3.add(label_18);
		
		txtFrameId = new JTextField();
		txtFrameId.setText((String) null);
		txtFrameId.setColumns(10);
		txtFrameId.setBounds(124, 14, 132, 20);
		panel_3.add(txtFrameId);
		
		txtCampaignId = new JTextField();
		txtCampaignId.setText((String) null);
		txtCampaignId.setColumns(10);
		txtCampaignId.setBounds(124, 38, 132, 20);
		panel_3.add(txtCampaignId);
		
		JPanel panel_4 = new JPanel();
		panel_4.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_4.setBackground(Color.WHITE);
		panel_4.setLayout(null);
		panel_4.setBounds(311, 72, 193, 113);
		panel_addNewFrame.add(panel_4);
		
		JLabel label_19 = new JLabel("Frame Height");
		label_19.setFont(new Font("Consolas", Font.PLAIN, 14));
		label_19.setBounds(10, 41, 100, 26);
		panel_4.add(label_19);
		
		txtFrameWidth = new JTextField();
		txtFrameWidth.setText("1120");
		txtFrameWidth.setColumns(10);
		txtFrameWidth.setBounds(122, 14, 48, 20);
		panel_4.add(txtFrameWidth);
		
		JLabel label_20 = new JLabel("Frame Width");
		label_20.setFont(new Font("Consolas", Font.PLAIN, 14));
		label_20.setBounds(10, 11, 100, 26);
		panel_4.add(label_20);
		
		JLabel label_21 = new JLabel("Rotate");
		label_21.setFont(new Font("Consolas", Font.PLAIN, 14));
		label_21.setBounds(10, 72, 48, 19);
		panel_4.add(label_21);
		
		txtRotate = new JTextField();
		txtRotate.setText("0");
		txtRotate.setColumns(10);
		txtRotate.setBounds(122, 75, 48, 20);
		panel_4.add(txtRotate);
		
		txtFrameHeight = new JTextField();
		txtFrameHeight.setBounds(120, 44, 48, 20);
		panel_4.add(txtFrameHeight);
		txtFrameHeight.setText("800");
		txtFrameHeight.setColumns(10);
		
		JPanel panel_5 = new JPanel();
		panel_5.setBackground(Color.WHITE);
		panel_5.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_5.setLayout(null);
		panel_5.setBounds(311, 196, 193, 87);
		panel_addNewFrame.add(panel_5);
		
		JLabel label_22 = new JLabel("Photo X ");
		label_22.setFont(new Font("Consolas", Font.PLAIN, 15));
		label_22.setBounds(10, 11, 74, 26);
		panel_5.add(label_22);
		
		txtPhotoX = new JTextField();
		txtPhotoX.setText((String) null);
		txtPhotoX.setColumns(10);
		txtPhotoX.setBounds(82, 14, 74, 20);
		panel_5.add(txtPhotoX);
		
		JLabel label_23 = new JLabel("Photo Y ");
		label_23.setFont(new Font("Consolas", Font.PLAIN, 15));
		label_23.setBounds(10, 44, 89, 26);
		panel_5.add(label_23);
		
		txtPhotoY = new JTextField();
		txtPhotoY.setText((String) null);
		txtPhotoY.setColumns(10);
		txtPhotoY.setBounds(82, 47, 74, 20);
		panel_5.add(txtPhotoY);
		
		JPanel panel_7 = new JPanel();
		panel_7.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_7.setBackground(Color.WHITE);
		panel_7.setLayout(null);
		panel_7.setBounds(311, 300, 193, 87);
		panel_addNewFrame.add(panel_7);
		
		JLabel label_24 = new JLabel("Scale X");
		label_24.setFont(new Font("Consolas", Font.PLAIN, 15));
		label_24.setBounds(10, 11, 74, 26);
		panel_7.add(label_24);
		
		txtScaleX = new JTextField();
		txtScaleX.setText((String) null);
		txtScaleX.setColumns(10);
		txtScaleX.setBounds(83, 14, 74, 20);
		panel_7.add(txtScaleX);
		
		JLabel label_25 = new JLabel("Scale Y");
		label_25.setFont(new Font("Consolas", Font.PLAIN, 15));
		label_25.setBounds(10, 48, 74, 26);
		panel_7.add(label_25);
		
		txtScaleY = new JTextField();
		txtScaleY.setText((String) null);
		txtScaleY.setColumns(10);
		txtScaleY.setBounds(83, 51, 74, 20);
		panel_7.add(txtScaleY);
		
		JButton btnDone = new JButton("Done");
		btnDone.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String frameId="", campaignId="", frameHeight="", frameWidth="", photoX="", photoY="", scaleX="", scaleY="", rotate="", thumbnail="", foreground="",background="";
				frameId=txtFrameId.getText().trim();
				campaignId=txtCampaignId.getText().trim();
				frameHeight=txtFrameHeight.getText().trim();
				frameWidth=txtFrameWidth.getText().trim();
				photoX=txtPhotoX.getText().trim();
				photoY=txtPhotoY.getText().trim();
				scaleX=txtScaleX.getText().trim();
				scaleY=txtScaleY.getText().trim();
				rotate=txtRotate.getText().trim();
				thumbnail=txtThumbnail.getText().trim();
				foreground=txtForeground.getText().trim();
				background=txtBackground.getText().trim();
				
				if(frameId.equals("")|| campaignId.equals("")|| frameHeight.equals("")|| frameWidth.equals("")|| photoX.equals("")||photoY.equals("")||scaleX.equals("")||scaleY.equals("")||rotate.equals("")||thumbnail.equals("")||foreground.equals("")||background.equals("") )
				{
					JOptionPane.showMessageDialog(null,"Field(s) are empty");
				}
				else
				{
					try {
						if(!DBO.UpdateSettings.isFrameIdExists(frameId))
						{
							DBO.UpdateSettings.addNewFrame(frameId, campaignId, frameHeight, frameWidth, photoX, photoY, scaleX, scaleY, rotate, thumbnail, foreground, background);
							comboBox.addItem(frameId);
						}
							else
							JOptionPane.showMessageDialog(null,"Frame Id already exists");
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
			}
		});
		btnDone.setBackground(Color.WHITE);
		btnDone.setBounds(99, 325, 89, 32);
		panel_addNewFrame.add(btnDone);
		
		JPanel panel_edit = new JPanel();
		panel_edit.setBackground(Color.WHITE);
		tabbedPane.addTab("Edit & Delete Frames", null, panel_edit, "Edit and Delete Frames");
		panel_edit.setLayout(null);
		final JPanel panel_9 = new JPanel();
		panel_9.setVisible(false);
		panel_9.setBackground(Color.WHITE);
		panel_9.setBounds(10, 110, 564, 299);
		panel_edit.add(panel_9);
		panel_9.setLayout(null);
		
		
		
		final JButton button_4 = new JButton("Delete");
		
		final JButton btn_DeleteAllFrames = new JButton("Delete All Frames");
		btn_DeleteAllFrames.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				int ans=JOptionPane.showConfirmDialog(null, "Do you want to delete all frames?");
				if(ans==0)
				{
				try {
					DBO.UpdateSettings.deleteAllFrames();
					panel_9.setVisible(false);
					button_4.setVisible(false);
					
					for(int i=0;i<frameId.length;i++)
						comboBox.removeItem(frameId[i]);
					//loadComboBox();
					btn_DeleteAllFrames.setVisible(false);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				}
			}
		});
		btn_DeleteAllFrames.setBackground(Color.WHITE);
		btn_DeleteAllFrames.setBounds(371, 74, 158, 34);
		panel_edit.add(btn_DeleteAllFrames);
		
		
		
		JPanel panel_10 = new JPanel();
		panel_10.setLayout(null);
		panel_10.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_10.setBackground(Color.WHITE);
		panel_10.setBounds(10, 11, 272, 104);
		panel_9.add(panel_10);
		
		JLabel label_26 = new JLabel("Thumbnail");
		label_26.setFont(new Font("Consolas", Font.PLAIN, 14));
		label_26.setBounds(10, 12, 72, 19);
		panel_10.add(label_26);
		
		textThumbnail = new JTextField();
		textThumbnail.setText((String) null);
		textThumbnail.setColumns(10);
		textThumbnail.setBounds(156, 11, 102, 20);
		panel_10.add(textThumbnail);
		
		JLabel label_27 = new JLabel("Foreground Frame");
		label_27.setFont(new Font("Consolas", Font.PLAIN, 14));
		label_27.setBounds(10, 42, 136, 26);
		panel_10.add(label_27);
		
		textForeground = new JTextField();
		textForeground.setText((String) null);
		textForeground.setColumns(10);
		textForeground.setBounds(156, 42, 102, 20);
		panel_10.add(textForeground);
		
		JLabel label_28 = new JLabel("Background Frame");
		label_28.setFont(new Font("Consolas", Font.PLAIN, 14));
		label_28.setBounds(10, 70, 138, 26);
		panel_10.add(label_28);
		
		textBackground = new JTextField();
		textBackground.setText((String) null);
		textBackground.setColumns(10);
		textBackground.setBounds(156, 73, 102, 20);
		panel_10.add(textBackground);
		
		JPanel panel_11 = new JPanel();
		panel_11.setLayout(null);
		panel_11.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_11.setBackground(Color.WHITE);
		panel_11.setBounds(361, 11, 193, 104);
		panel_9.add(panel_11);
		
		JLabel label_29 = new JLabel("Frame Height");
		label_29.setFont(new Font("Consolas", Font.PLAIN, 14));
		label_29.setBounds(10, 41, 100, 26);
		panel_11.add(label_29);
		
		textFrameWidth = new JTextField();
		textFrameWidth.setText("1120");
		textFrameWidth.setColumns(10);
		textFrameWidth.setBounds(122, 14, 48, 20);
		panel_11.add(textFrameWidth);
		
		JLabel label_30 = new JLabel("Frame Width");
		label_30.setFont(new Font("Consolas", Font.PLAIN, 14));
		label_30.setBounds(10, 11, 100, 26);
		panel_11.add(label_30);
		
		JLabel label_31 = new JLabel("Rotate");
		label_31.setFont(new Font("Consolas", Font.PLAIN, 14));
		label_31.setBounds(10, 72, 48, 19);
		panel_11.add(label_31);
		
		textRotate = new JTextField();
		textRotate.setText("0");
		textRotate.setColumns(10);
		textRotate.setBounds(122, 75, 48, 20);
		panel_11.add(textRotate);
		
		textFrameHeight = new JTextField();
		textFrameHeight.setText("800");
		textFrameHeight.setColumns(10);
		textFrameHeight.setBounds(120, 44, 48, 20);
		panel_11.add(textFrameHeight);
		
		JPanel panel_12 = new JPanel();
		panel_12.setLayout(null);
		panel_12.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_12.setBackground(Color.WHITE);
		panel_12.setBounds(361, 126, 193, 79);
		panel_9.add(panel_12);
		
		JLabel label_32 = new JLabel("Photo X ");
		label_32.setFont(new Font("Consolas", Font.PLAIN, 15));
		label_32.setBounds(10, 11, 74, 26);
		panel_12.add(label_32);
		
		textPhotoX = new JTextField();
		textPhotoX.setText((String) null);
		textPhotoX.setColumns(10);
		textPhotoX.setBounds(94, 14, 74, 20);
		panel_12.add(textPhotoX);
		
		JLabel label_33 = new JLabel("Photo Y ");
		label_33.setFont(new Font("Consolas", Font.PLAIN, 15));
		label_33.setBounds(10, 44, 89, 26);
		panel_12.add(label_33);
		
		textPhotoY = new JTextField();
		textPhotoY.setText((String) null);
		textPhotoY.setColumns(10);
		textPhotoY.setBounds(94, 47, 74, 20);
		panel_12.add(textPhotoY);
		
		JPanel panel_13 = new JPanel();
		panel_13.setLayout(null);
		panel_13.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_13.setBackground(Color.WHITE);
		panel_13.setBounds(10, 126, 272, 69);
		panel_9.add(panel_13);
		
		JLabel label_34 = new JLabel("Frame Id");
		label_34.setFont(new Font("Consolas", Font.PLAIN, 14));
		label_34.setBounds(14, 11, 100, 26);
		panel_13.add(label_34);
		
		JLabel label_35 = new JLabel("Campaign Id");
		label_35.setFont(new Font("Consolas", Font.PLAIN, 14));
		label_35.setBounds(14, 35, 100, 26);
		panel_13.add(label_35);
		
		textFrameId = new JTextField();
		textFrameId.setText((String) null);
		textFrameId.setColumns(10);
		textFrameId.setBounds(124, 14, 132, 20);
		panel_13.add(textFrameId);
		
		textCampaignId = new JTextField();
		textCampaignId.setText((String) null);
		textCampaignId.setColumns(10);
		textCampaignId.setBounds(124, 38, 132, 20);
		panel_13.add(textCampaignId);
		
		JPanel panel_14 = new JPanel();
		panel_14.setLayout(null);
		panel_14.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_14.setBackground(Color.WHITE);
		panel_14.setBounds(361, 216, 193, 72);
		panel_9.add(panel_14);
		
		JLabel label_36 = new JLabel("Scale X");
		label_36.setFont(new Font("Consolas", Font.PLAIN, 15));
		label_36.setBounds(10, 11, 74, 26);
		panel_14.add(label_36);
		
		textScaleX = new JTextField();
		textScaleX.setText((String) null);
		textScaleX.setColumns(10);
		textScaleX.setBounds(94, 14, 74, 20);
		panel_14.add(textScaleX);
		
		JLabel label_37 = new JLabel("Scale Y");
		label_37.setFont(new Font("Consolas", Font.PLAIN, 15));
		label_37.setBounds(10, 48, 74, 26);
		panel_14.add(label_37);
		
		textScaleY = new JTextField();
		textScaleY.setText((String) null);
		textScaleY.setColumns(10);
		textScaleY.setBounds(94, 51, 74, 20);
		panel_14.add(textScaleY);
		
		
		button_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				int ans=JOptionPane.showConfirmDialog(null, "Do you want to delete frame?");
				if(ans==0)
				{
				try {
					DBO.UpdateSettings.deleteSelectedFrame(textFrameId.getText().trim());
					panel_9.setVisible(false);
					//loadComboBox();
					comboBox.removeItem(textFrameId.getText());
					if(DBO.UpdateSettings.countFrames()<1)
					{
						btn_DeleteAllFrames.setVisible(false);
						JOptionPane.showMessageDialog(null,"No frame Configured!");
					}
					
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				}
			}
		});
		button_4.setBackground(Color.WHITE);
		button_4.setBounds(55, 233, 89, 32);
		panel_9.add(button_4);
		
		JButton button_5 = new JButton("Update");
		button_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String frameId = null, campaignId = null, frameHeight= null, frameWidth= null, photoX= null, photoY= null, scaleX= null, scaleY = null, rotate= null, thumbnail= null, foreground= null, background= null;
				frameId=textFrameId.getText().trim();
				campaignId=textCampaignId.getText().trim();
				frameHeight=textFrameHeight.getText().trim();
				frameWidth=textFrameWidth.getText().trim();
				photoX=textPhotoX.getText().trim();
				photoY=textPhotoY.getText().trim();
				scaleX=textScaleX.getText().trim();
				scaleY=textScaleY.getText().trim();
				rotate=textRotate.getText().trim();
				thumbnail=textThumbnail.getText().trim();
				foreground=textForeground.getText().trim();
				background=textBackground.getText().trim();
				
				if(frameId.equals("")|| campaignId.equals("")|| frameHeight.equals("")|| frameWidth.equals("")|| photoX.equals("")||photoY.equals("")||scaleX.equals("")||scaleY.equals("")||rotate.equals("")||thumbnail.equals("")||foreground.equals("")||background.equals("") )
				{
					JOptionPane.showMessageDialog(null,"Field(s) are empty");
				}
				else
				{	
				
					try {
						DBO.UpdateSettings.updateFrame(frameId, campaignId, frameHeight, frameWidth, photoX, photoY, scaleX, scaleY, rotate, thumbnail, foreground, background);
						
					} catch (SQLException eee) {
						// TODO Auto-generated catch block
						eee.printStackTrace();
					}
				}
			}
		});
		button_5.setBackground(Color.WHITE);
		button_5.setBounds(169, 233, 89, 32);
		panel_9.add(button_5);
		
		
		
		
		comboBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				if(!comboBox.getSelectedItem().toString().trim().equals("Select Frame ID"))
				{
					panel_9.setVisible(true);
					//JOptionPane.showMessageDialog(null, "hit");
					String selectedFrameId=comboBox.getSelectedItem().toString().trim();
					
					
					
					String query="select CampaignId," +
					"FinalFrameHeight," +
					"FinalFrameWidth," +
					"PhotoX," +
					"PhotoY," +
					"ScaleX," +
					"ScaleY," +
					"Rotate," +
					"Thumbnail," +
					"ForegroundFrame," +
					"BackgroundFrame from campaign_image_frame_configuration where FrameId='"+selectedFrameId+"'";
					Connection con=null;
					Statement stmt=null;
					try{
						textFrameId.setText(selectedFrameId);
						con=DBUtil.getConnection();
						stmt=con.createStatement();
						ResultSet res=null;
						res=stmt.executeQuery(query);
						
						while(res.next())
						{
							textCampaignId.setText(res.getString(1));
							textFrameWidth.setText(res.getString(3));
							textPhotoX.setText(res.getString(4));
							textPhotoY.setText(res.getString(5));
							textScaleX.setText(res.getString(6));
							textScaleY.setText(res.getString(7));
							textRotate.setText(res.getString(8));
							textThumbnail.setText(res.getString(9));
							textForeground.setText(res.getString(10));
							textBackground.setText(res.getString(11));
							
						}
					//	System.out.println(frames);
					}
						catch(Exception e)
						{
							System.out.println("Error Occured while fetching frames::"+e.getMessage());
							JOptionPane.showMessageDialog(null,"Error Occured :: "+e.getMessage());
						}
						finally
						{
							if(stmt!=null)
							{
								try {
									stmt.close();
								} catch (SQLException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
							
							if(con!=null)
							{
								try {
									con.close();
								} catch (SQLException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						}
					
					
					
					
				}
					else
					{
					panel_9.setVisible(false);
					JOptionPane.showMessageDialog(null, "Please Select Frame Id");
					}
					// TODO Auto-generated method stub
				
			}
		});
		
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Select Frame ID"}));
		comboBox.setFont(new Font("Consolas", Font.PLAIN, 14));
		comboBox.setBackground(Color.WHITE);
		comboBox.setBounds(10, 74, 186, 34);
		panel_edit.add(comboBox);
		
		
		
		JPanel panel_8 = new JPanel();
		panel_8.setBackground(Color.WHITE);
		tabbedPane.addTab("User Data", null, panel_8, "View User's Photo Delivery Status");
		panel_8.setLayout(null);
		
		final JLabel lbl_totalUserEntries = new JLabel((String) null);
		lbl_totalUserEntries.setForeground(new Color(0, 100, 0));
		lbl_totalUserEntries.setFont(new Font("Consolas", Font.PLAIN, 16));
		lbl_totalUserEntries.setBounds(331, 105, 64, 28);
		panel_8.add(lbl_totalUserEntries);
		
		JLabel label_39 = new JLabel("Total User Entries");
		label_39.setFont(new Font("Consolas", Font.PLAIN, 16));
		label_39.setBounds(133, 105, 169, 28);
		panel_8.add(label_39);
		
		JLabel label_40 = new JLabel("Uploaded");
		label_40.setFont(new Font("Consolas", Font.PLAIN, 16));
		label_40.setBounds(133, 154, 90, 28);
		panel_8.add(label_40);
		
		final JLabel lbl_uploaded = new JLabel((String) null);
		lbl_uploaded.setForeground(new Color(0, 100, 0));
		lbl_uploaded.setFont(new Font("Consolas", Font.PLAIN, 16));
		lbl_uploaded.setBounds(331, 154, 64, 28);
		panel_8.add(lbl_uploaded);
		
		JLabel label_42 = new JLabel("Mail Sent");
		label_42.setFont(new Font("Consolas", Font.PLAIN, 16));
		label_42.setBounds(133, 204, 107, 28);
		panel_8.add(label_42);
		
		final JLabel lbl_mailSent = new JLabel((String) null);
		lbl_mailSent.setForeground(new Color(0, 100, 0));
		lbl_mailSent.setFont(new Font("Consolas", Font.PLAIN, 16));
		lbl_mailSent.setBounds(331, 204, 64, 28);
		panel_8.add(lbl_mailSent);
		
		JLabel label_44 = new JLabel("Prints");
		label_44.setFont(new Font("Consolas", Font.PLAIN, 16));
		label_44.setBounds(133, 256, 76, 28);
		panel_8.add(label_44);
		
		final JLabel lbl_prints = new JLabel((String) null);
		lbl_prints.setForeground(new Color(0, 100, 0));
		lbl_prints.setFont(new Font("Consolas", Font.PLAIN, 16));
		lbl_prints.setBounds(331, 256, 64, 28);
		panel_8.add(lbl_prints);
		
		JButton btnClearUserData = new JButton("Clear User Data");
		btnClearUserData.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int ans=JOptionPane.showConfirmDialog(null, "Do you want to clear user data?");
				if(ans==0)
				{
				try {
					DBO.UpdateSettings.clearUserData();
					DAO.UserDAO.userTableReader();
					lbl_mailSent.setText(DTO.UserDTO.getTotalMailSent());
					lbl_prints.setText(DTO.UserDTO.getTotalPrinted());
					lbl_totalUserEntries.setText(DTO.UserDTO.getTotalPhotographs());
					lbl_uploaded.setText(UserDTO.getPendingForUpload());
					
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				}
			}
		});
		btnClearUserData.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnClearUserData.setBackground(Color.WHITE);
		btnClearUserData.setBounds(223, 317, 151, 28);
		panel_8.add(btnClearUserData);
		
		lbl_mailSent.setText(DTO.UserDTO.getTotalMailSent());
		lbl_prints.setText(DTO.UserDTO.getTotalPrinted());
		lbl_totalUserEntries.setText(DTO.UserDTO.getTotalPhotographs());
		lbl_uploaded.setText(UserDTO.getPendingForUpload());
		
		if(DBO.UpdateSettings.countFrames()<1)
		{
			btn_DeleteAllFrames.setVisible(false);
			JOptionPane.showMessageDialog(null,"No frame Configured!");
		}
			else
				loadComboBox();
	}
	
	public void loadComboBox()
	{
		try {
			
			int lenght=DBO.UpdateSettings.frameList().length;
		 frameId=DBO.UpdateSettings.frameList();
			for(int i=0;i<lenght;i++)
			{
				comboBox.addItem(frameId[i]);
			}
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
