package DAO;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import DTO.MachineIdDTO;
import Utility.DBUtil;

public class MachineConfigFileDAO {
	
	//Reading Machine Core
	public static void loadMachineCore() throws SQLException
	{
		
		String query="select MachineHealthTimePattern from machine_core";
		Connection con=null;
		Statement stmt=null;
		try{
			con=DBUtil.getConnection();
			stmt=con.createStatement();
			ResultSet res=stmt.executeQuery(query);
			while(res.next())
			{
				
			MachineIdDTO.setMachineHealthTimePattern(res.getString(1));
			//	System.out.println("Loaded Health Time Pattern "+MachineIdDTO.getTimePattern());
				 
				
			}
		
		}
		catch(Exception e)
		{
			System.out.println("Error Occured in DAO::"+e.getMessage());
			
		}
		finally
		{
			if(stmt!=null)
			{
				stmt.close();
			}
			
			if(con!=null)
			{
				con.close();
			}
		}
	}
	
	
	
	
	//Reading Machine Configuration
	public static void loadMachineConfig() throws SQLException
	{
		
		String query="select SeeCameraName," +
				"CameraID," +
				"CameraResX," +
				"CameraResY," +
				"ShowDOBScreen," +
				"AskForName," +
				"AskForPhone," +
				"CaptureVideo," +
				"ScreenSaverMedia," +
				"ScreenSaverTime," +
				"PhonePrefix," +
				"MmsPhonePrefix," +
				"TakePhotoCounterTiming," +
				"ShowPrintButtonOnFinalFramePage," +
				"ShowSmsButtonOnFinalFramePage," +
				"ShowMmsButtonOnFinalFramePage," +
				"ShowEmailButtonOnFinalFramePage," +
				"ShowFacebookShareButtonOnFinalFramePage," +
				"ShowTwitterButtonOnFinalFramePage," +
				"ShowFeedBackScreen," +
				"ImageMagickPath,MachineId  from campaign_machine_configuration";
		Connection con=null;
		Statement stmt=null;
		try{
			con=DBUtil.getConnection();
			stmt=con.createStatement();
			ResultSet res=stmt.executeQuery(query);
			while(res.next())
			{
				
			MachineIdDTO.setSeeCameraName(res.getString(1));
			MachineIdDTO.setCameraId(res.getString(2));
			MachineIdDTO.setCameraResX(res.getString(3));
			MachineIdDTO.setCameraResY(res.getString(4));
			MachineIdDTO.setDobScreen(res.getString(5));
			MachineIdDTO.setAskForName(res.getString(6));
			MachineIdDTO.setAskForPhone(res.getString(7));
			MachineIdDTO.setCaptureVideo(res.getString(8));
			MachineIdDTO.setScreensaverType(res.getString(9));
			MachineIdDTO.setScreensaverTime(res.getString(10));
			MachineIdDTO.setSmsPhonePrefix(res.getString(11));
			MachineIdDTO.setMmsPhonePrefix(res.getString(12));
			MachineIdDTO.setPhotoCounterTiming(res.getString(13));
			MachineIdDTO.setPrintButton(res.getString(14));
			MachineIdDTO.setSmsButton(res.getString(15));
			MachineIdDTO.setMmsButton(res.getString(16));
			MachineIdDTO.setEmailButton(res.getString(17));
			MachineIdDTO.setFacebookButton(res.getString(18));
			MachineIdDTO.setTwitterButton(res.getString(19));
			MachineIdDTO.setFeedbackScreen(res.getString(20));
			MachineIdDTO.setImageMagickPath(res.getString(21));
			MachineIdDTO.setMachineId(res.getString(22));
			}
		
		}
		catch(Exception e)
		{
			System.out.println("Error Occured in DAO::"+e.getMessage());
			
		}
		finally
		{
			if(stmt!=null)
			{
				stmt.close();
			}
			
			if(con!=null)
			{
				con.close();
			}
		}
	}

	
	
	public static void loadMachineMapping() throws SQLException
	{
		
		String query="select CampaignId,CampaignName from campaign_machine_mapping";
		Connection con=null;
		Statement stmt=null;
		try{
			con=DBUtil.getConnection();
			stmt=con.createStatement();
			ResultSet res=stmt.executeQuery(query);
			while(res.next())
			{
				
			MachineIdDTO.setMappingCampaignId(res.getString(1));
			MachineIdDTO.setMappingCampaignName(res.getString(2));
			
			//	System.out.println("Loaded Health Time Pattern "+MachineIdDTO.getTimePattern());
				 
				
			}
		
		}
		catch(Exception e)
		{
			System.out.println("Error Occured in DAO::"+e.getMessage());
			
		}
		finally
		{
			if(stmt!=null)
			{
				stmt.close();
			}
			
			if(con!=null)
			{
				con.close();
			}
		}
	}
	
	
	
}
