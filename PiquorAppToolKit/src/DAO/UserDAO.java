package DAO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import DTO.UserDTO;
import Utility.DBUtil;

public class UserDAO {
	
	public static void userTableReader()throws SQLException
	{
		
		String queryTotalPhotographs="select count(*) from user";
		String queryTotalMailSent="select count(*) from user where mailsent='1'";
		String queryPendingForUpload="select count(*) from user where fileUpload='1'";
		String queryPendingForMail="select count(*) from user where mailsent='0'";
		String queryTotalPrinted="select count(*) from user where IsPrinted='1'";
		Connection con=null;
		Statement stmt=null;
		try{
			con=DBUtil.getConnection();
			stmt=con.createStatement();
			ResultSet res=null;
			res=stmt.executeQuery(queryTotalPhotographs);
			while(res.next())
			{
				UserDTO.setTotalPhotographs(String.valueOf(res.getInt(1)).trim());
			}
			
			
			res=stmt.executeQuery(queryTotalMailSent);
			while(res.next())
			{
				UserDTO.setTotalMailSent(String.valueOf(res.getInt(1)).trim());
			}
			
			
			res=stmt.executeQuery(queryTotalPrinted);
			while(res.next())
			{
				UserDTO.setTotalPrinted(String.valueOf(res.getInt(1)).trim());
			}
			
			
			res=stmt.executeQuery(queryPendingForUpload);
			while(res.next())
			{
				UserDTO.setPendingForUpload(String.valueOf(res.getInt(1)).trim());
			}
			
			
			
			res=stmt.executeQuery(queryPendingForMail);
			while(res.next())
			{
				UserDTO.setPendingForMail(String.valueOf(res.getInt(1)).trim());
			}
		
		}
		catch(Exception e)
		{
			System.out.println("Error Occured in DAO::"+e.getMessage());
			
		}
		finally
		{
			if(stmt!=null)
			{
				stmt.close();
			}
			
			if(con!=null)
			{
				con.close();
			}
		}
	}

}
