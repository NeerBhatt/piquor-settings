package Utility;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

import org.sqlite.SQLiteConfig;

public class DBUtil {
	public static Connection getConnection()
	{
	   String connectionUrl="jdbc:sqlite:C:\\Piquor\\piquorAppDatabase.db";
	   String driverClass ="org.sqlite.JDBC";
	  
	   Connection con = null;
	   try{
	  /* Class.forName(driverClass);
	   con=DriverManager.getConnection(connectionUrl,userName,password);
	   */
		   Class.forName(driverClass);
		   Properties prop=new Properties();
			
		   SQLiteConfig config=new SQLiteConfig();
			config.setSharedCache(true);
			con=DriverManager.getConnection(connectionUrl,config.toProperties());
	   }
	   catch (Exception ee)
	   {
		   System.out.println("Exception Occured in DBUTIL::"+ee.getMessage());
		  
	   }
	   return con;
	}
}
