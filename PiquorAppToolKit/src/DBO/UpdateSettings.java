package DBO;

import java.lang.reflect.Array;
import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import DTO.UserDTO;
import Utility.DBUtil;

public class UpdateSettings {

	public static void updatePiquorAppFeatures(String email,String facebook,String twitter,String print,String sms,String mms,String askforname,String askforPhone,String feedback,String dob,String recordVideo)throws SQLException
	{

		String query="Update campaign_machine_configuration set ShowPrintButtonOnFinalFramePage='"+print+"'," +
				"ShowSmsButtonOnFinalFramePage='"+sms+"'," +
				"ShowMmsButtonOnFinalFramePage='"+mms+"'," +
				"ShowEmailButtonOnFinalFramePage='"+email+"'," +
				"ShowFacebookShareButtonOnFinalFramePage='"+facebook+"'," +
				"ShowTwitterButtonOnFinalFramePage='"+twitter+"'," +
				"ShowFeedBackScreen='"+feedback+"',"+
				"ShowDOBScreen='"+dob+"'," +
				"AskForName='"+askforname+"'," +
				"AskForPhone='"+askforPhone+"'," +
						"CaptureVideo='"+recordVideo+"'";
		Connection con=null;
		Statement stmt=null;
		try{
			con=DBUtil.getConnection();
			stmt=con.createStatement();
			stmt.executeUpdate(query);
			JOptionPane.showMessageDialog(null, "Settings Updated!");
		}
		catch(Exception e)
		{
			System.out.println("Error Occured :: "+e.getMessage());
			
		}
		finally
		{
			if(stmt!=null)
			{
				stmt.close();
			}
			
			if(con!=null)
			{
				con.close();
			}
		}
	}

	
	
	public static void updateCameraSettings(String machineId,String seeCameraName,String CameraId,String PhotoCounterTiming,String CameraResX,String CameraResY )throws SQLException
	{

		String query="Update campaign_machine_configuration set MachineId='" +machineId+"' ,"+
				"SeeCameraName='" +seeCameraName+"',"+
				"CameraID='" +CameraId+"',"+
				"CameraResX='" +CameraResX+"',"+
				"CameraResY='" +CameraResY+"',"+
				"TakePhotoCounterTiming='" +PhotoCounterTiming+"'";
		Connection con=null;
		Statement stmt=null;
		try{
			con=DBUtil.getConnection();
			stmt=con.createStatement();
			stmt.executeUpdate(query);
			JOptionPane.showMessageDialog(null, "Settings Updated!");
		}
		catch(Exception e)
		{
			System.out.println("Error Occured :: "+e.getMessage());
			
		}
		finally
		{
			if(stmt!=null)
			{
				stmt.close();
			}
			
			if(con!=null)
			{
				con.close();
			}
		}
	}

	
	public static void updateOtherSettings(String smsPhonePrefix,String mmsPhonePrefix,String screensaverType,String ScreenSaverTime,String imageMagickPath)throws SQLException
	{

		String query="Update campaign_machine_configuration set " +
				"ScreenSaverMedia='" +screensaverType+"',"+
				"ScreenSaverTime='" +ScreenSaverTime+"',"+
				"PhonePrefix='" +smsPhonePrefix+"',"+
				"MmsPhonePrefix='" +mmsPhonePrefix+"',"+
				"ImageMagickPath='" +imageMagickPath+"'";
		Connection con=null;
		Statement stmt=null;
		try{
			con=DBUtil.getConnection();
			stmt=con.createStatement();
			stmt.executeUpdate(query);
			JOptionPane.showMessageDialog(null, "Settings Updated!");
		}
		catch(Exception e)
		{
			System.out.println("Error Occured :: "+e.getMessage());
			
		}
		finally
		{
			if(stmt!=null)
			{
				stmt.close();
			}
			
			if(con!=null)
			{
				con.close();
			}
		}
	}
	
	
	public static void addNewFrame(String frameId,String campaignId,String frameHeight,String frameWidth,String photoX,String photoY,String scaleX,String scaleY,String rotate,String thumbnail,String foreground,String background )throws SQLException
	{
		/*String query="insert into campaign_image_configuration(FrameID," +
				"CampaignId," +
				"FinalFrameHeight," +
				"FinalFrameWidth," +
				"PhotoX," +
				"PhotoY," +
				"ScaleX," +
				"ScaleY," +
				"Rotate," +
				"Thumbnail," +
				"ForegroundFrame," +
				"BackgroundFrame," +
				"ImageOnTopX," +
				"ImageOnTopY) values()";*/
		Connection con=null;
		PreparedStatement stmt=null;
		try{
			con=DBUtil.getConnection();
			stmt=con.prepareStatement("insert into campaign_image_frame_configuration(FrameID," +
					"CampaignId," +
					"FinalFrameHeight," +
					"FinalFrameWidth," +
					"PhotoX," +
					"PhotoY," +
					"ScaleX," +
					"ScaleY," +
					"Rotate," +
					"Thumbnail," +
					"ForegroundFrame," +
					"BackgroundFrame," +
					"ImageOnTopX," +
					"ImageOnTopY) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			
			stmt.setString(1,frameId);
			stmt.setString(2,campaignId);
			stmt.setString(3,frameHeight);
			stmt.setString(4,frameWidth);
			stmt.setString(5,photoX);
			stmt.setString(6,photoY);
			stmt.setString(7,scaleX);
			stmt.setString(8,scaleY);
			stmt.setString(9,rotate);
			stmt.setString(10,thumbnail);
			stmt.setString(11,foreground);
			stmt.setString(12,background);
			stmt.setString(13,"0");
			stmt.setString(14,"0");

			stmt.executeUpdate();
			JOptionPane.showMessageDialog(null, "Frame Added Successfully!");
		}
		catch(Exception e)
		{
			System.out.println("Error Occured in DAO::"+e.getMessage());
			JOptionPane.showMessageDialog(null,"Error Occured ::"+e.getMessage());
			
		}
		finally
		{
			if(stmt!=null)
			{
				stmt.close();
			}
			
			if(con!=null)
			{
				con.close();
			}
		}
	}
	
	
	public static boolean isFrameIdExists(String frameId) throws SQLException
	{
		boolean result=false;
		System.out.println("FrameId "+frameId);
		String query="select CASE WHEN count(1) > 0 THEN 'true' ELSE 'false' END from campaign_image_frame_configuration where FrameId='"+frameId+"'";
		Connection con=null;
		Statement stmt=null;
		try{
			con=DBUtil.getConnection();
			stmt=con.createStatement();
			ResultSet res=null;
			res=stmt.executeQuery(query);
			while(res.next())
			{
				result=Boolean.parseBoolean(res.getString(1));
				
			}
		}
			catch(Exception e)
			{
				System.out.println("Error Occured ::"+e.getMessage());
				JOptionPane.showMessageDialog(null,"Error Occured :: "+e.getMessage());
			}
			finally
			{
				if(stmt!=null)
				{
					stmt.close();
				}
				
				if(con!=null)
				{
					con.close();
				}
			}
		//System.out.println(result);
		return result;
	}
	
	public static String[] frameList() throws SQLException
	{
		String frames[] = new String[countFrames()];
		
		
		String query="select FrameId from campaign_image_frame_configuration";
		Connection con=null;
		Statement stmt=null;
		try{
			con=DBUtil.getConnection();
			stmt=con.createStatement();
			ResultSet res=null;
			res=stmt.executeQuery(query);
			int i=0;
			while(res.next())
			{
				//System.out.println(res.getString(1));
				frames[i]=res.getString(1);
				i++;
			}
		//	System.out.println(frames);
		}
			catch(Exception e)
			{
				System.out.println("Error Occured while fetching frames::"+e.getMessage());
				JOptionPane.showMessageDialog(null,"Error Occured :: "+e.getMessage());
			}
			finally
			{
				if(stmt!=null)
				{
					stmt.close();
				}
				
				if(con!=null)
				{
					con.close();
				}
			}
		
		return frames;
	}
	
	public static int countFrames() throws SQLException
	{
		int frameCount=0;
		String query="select count(*) from campaign_image_frame_configuration";
		Connection con=null;
		Statement stmt=null;
		try{
			
			con=DBUtil.getConnection();
			stmt=con.createStatement();
			ResultSet res=null;
			res=stmt.executeQuery(query);
			while(res.next())
			{
				frameCount=res.getInt(1);
			}
		}
			catch(Exception e)
			{
				System.out.println("Error Occured while counting frames::"+e.getMessage());
				JOptionPane.showMessageDialog(null,"Error Occured :: "+e.getMessage());
			}
			finally
			{
				if(stmt!=null)
				{
					stmt.close();
				}
				
				if(con!=null)
				{
					con.close();
				}
			}
		
		return frameCount;
	}
	
	public static void updateFrame(String frameId,String campaignId,String frameHeight,String frameWidth,String photoX,String photoY,String scaleX,String scaleY,String rotate,String thumbnail,String foreground,String background )throws SQLException
	{
		/*String query="insert into campaign_image_configuration(FrameID," +
				"CampaignId," +
				"FinalFrameHeight," +
				"FinalFrameWidth," +
				"PhotoX," +
				"PhotoY," +
				"ScaleX," +
				"ScaleY," +
				"Rotate," +
				"Thumbnail," +
				"ForegroundFrame," +
				"BackgroundFrame," +
				"ImageOnTopX," +
				"ImageOnTopY) values()";*/
		Connection con=null;
		PreparedStatement stmt=null;
		try{
			con=DBUtil.getConnection();
			stmt=con.prepareStatement("update campaign_image_frame_configuration set FrameID='"+frameId+"'," +
					"CampaignId='"+campaignId+"'," +
					"FinalFrameHeight='"+frameHeight+"'," +
					"FinalFrameWidth='"+frameWidth+"'," +
					"PhotoX='"+photoX+"'," +
					"PhotoY='"+photoY+"'," +
					"ScaleX='"+scaleX+"'," +
					"ScaleY='"+scaleY+"'," +
					"Rotate='"+rotate+"'," +
					"Thumbnail='"+thumbnail+"'," +
					"ForegroundFrame='"+foreground+"'," +
					"BackgroundFrame='"+background+"' " +
					" where FrameId='"+frameId+"'") ;
			stmt.executeUpdate();
			JOptionPane.showMessageDialog(null, "Frame Updated Successfully!");
		}
		catch(Exception e)
		{
			System.out.println("Error Occured in DAO::"+e.getMessage());
			JOptionPane.showMessageDialog(null,"Error Occured ::"+e.getMessage());
			
		}
		finally
		{
			if(stmt!=null)
			{
				stmt.close();
			}
			
			if(con!=null)
			{
				con.close();
			}
		}
	}
	
	
	public static void deleteSelectedFrame(String frameId) throws SQLException
	{
		
		String query="delete from campaign_image_frame_configuration where FrameId='"+frameId+"'";
		Connection con=null;
		Statement stmt=null;
		try{
			
			con=DBUtil.getConnection();
			stmt=con.createStatement();
			stmt.executeUpdate(query);
			JOptionPane.showMessageDialog(null,"Frame Deleted Successfully!");
		}
			catch(Exception e)
			{
				System.out.println("Error Occured while Deleting frame::"+e.getMessage());
				JOptionPane.showMessageDialog(null,"Error Occured :: "+e.getMessage());
			}
			finally
			{
				if(stmt!=null)
				{
					stmt.close();
				}
				
				if(con!=null)
				{
					con.close();
				}
			}
	
	}
	
	public static void deleteAllFrames() throws SQLException
	{
		
		String query="delete from campaign_image_frame_configuration";
		Connection con=null;
		Statement stmt=null;
		try{
			
			con=DBUtil.getConnection();
			stmt=con.createStatement();
			stmt.executeUpdate(query);
			JOptionPane.showMessageDialog(null,"All Frames Deleted Successfully!");
		}
			catch(Exception e)
			{
				System.out.println("Error Occured while Deleting frames::"+e.getMessage());
				JOptionPane.showMessageDialog(null,"Error Occured :: "+e.getMessage());
			}
			finally
			{
				if(stmt!=null)
				{
					stmt.close();
				}
				
				if(con!=null)
				{
					con.close();
				}
			}
	
	}
	
	
	public static void clearUserData() throws SQLException
	{
		
		String query="delete from user";
		Connection con=null;
		Statement stmt=null;
		try{
			
			con=DBUtil.getConnection();
			stmt=con.createStatement();
			stmt.executeUpdate(query);
			JOptionPane.showMessageDialog(null,"User data cleared!");
		}
			catch(Exception e)
			{
				System.out.println("Error Occured while Deleting frames::"+e.getMessage());
				JOptionPane.showMessageDialog(null,"Error Occured :: "+e.getMessage());
			}
			finally
			{
				if(stmt!=null)
				{
					stmt.close();
				}
				
				if(con!=null)
				{
					con.close();
				}
			}
		
	}
	
	public static void updateMachineCore(String MachineId) throws SQLException
	{
		
		String query="update machine_core set MachineId='"+MachineId+"'";
		Connection con=null;
		Statement stmt=null;
		try{
			
			con=DBUtil.getConnection();
			stmt=con.createStatement();
			stmt.executeUpdate(query);
		}
			catch(Exception e)
			{
				System.out.println("Error Occured while updating Machine Core::"+e.getMessage());
				JOptionPane.showMessageDialog(null,"Error Occured :: "+e.getMessage());
			}
			finally
			{
				if(stmt!=null)
				{
					stmt.close();
				}
				
				if(con!=null)
				{
					con.close();
				}
			}
	
	}
	
	
	
	
	
	public static void updateMachineMapping(String MachineId,String CampaignId, String CampaignName) throws SQLException
	{
		
		String query="update campaign_machine_mapping set MachineId='"+MachineId+"', CampaignId='"+CampaignId+"', CampaignName='"+CampaignName+"'";
		Connection con=null;
		Statement stmt=null;
		try{
			
			con=DBUtil.getConnection();
			stmt=con.createStatement();
			stmt.executeUpdate(query);
		}
			catch(Exception e)
			{
				System.out.println("Error Occured while updating machine mapping::"+e.getMessage());
				JOptionPane.showMessageDialog(null,"Error Occured :: "+e.getMessage());
			}
			finally
			{
				if(stmt!=null)
				{
					stmt.close();
				}
				
				if(con!=null)
				{
					con.close();
				}
			}
	
	}
	
	
	
}
