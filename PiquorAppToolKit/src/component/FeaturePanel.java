package component;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.JPanel;

public class FeaturePanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;



	/**
	 * Create the panel.
	 */
	public FeaturePanel() {
		
//		setOpaque(false);
	}
	
	 Image background = Toolkit.getDefaultToolkit().createImage("Images/panel_bck.png");
	
	@Override
	 protected void paintComponent(Graphics g)	 {
		
		System.out.println("Image Found Painting Component");
	  super.paintComponent(g); 
	 
	  if (background != null){
	        g.drawImage(background, 0,0,this.getWidth(),this.getHeight(),this);
	  }
	 } 
}
