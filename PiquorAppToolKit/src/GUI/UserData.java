package GUI;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import DTO.UserDTO;

public class UserData extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UserData frame = new UserData();
					frame.setVisible(true);
					frame.setLocationRelativeTo ( null );
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws SQLException 
	 */
	public UserData() throws SQLException {
		DAO.UserDAO.userTableReader();
		setMinimumSize(new Dimension(400, 400));
		setPreferredSize(new Dimension(400, 400));
		setMaximumSize(new Dimension(400, 400));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 409, 349);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblTotalUserEntries = new JLabel("Total User Entries");
		lblTotalUserEntries.setFont(new Font("Consolas", Font.PLAIN, 16));
		lblTotalUserEntries.setBounds(45, 38, 169, 28);
		contentPane.add(lblTotalUserEntries);
		
		JLabel lblMailSent = new JLabel("Mail Sent");
		lblMailSent.setFont(new Font("Consolas", Font.PLAIN, 16));
		lblMailSent.setBounds(45, 137, 107, 28);
		contentPane.add(lblMailSent);
		
		JLabel lblUploaded = new JLabel("Uploaded");
		lblUploaded.setFont(new Font("Consolas", Font.PLAIN, 16));
		lblUploaded.setBounds(45, 87, 90, 28);
		contentPane.add(lblUploaded);
		
		JLabel lblPrints = new JLabel("Prints");
		lblPrints.setFont(new Font("Consolas", Font.PLAIN, 16));
		lblPrints.setBounds(45, 189, 76, 28);
		contentPane.add(lblPrints);
		
		final JLabel lbl_totalUserEntries = new JLabel("Prints");
		lbl_totalUserEntries.setForeground(new Color(0, 100, 0));
		lbl_totalUserEntries.setFont(new Font("Consolas", Font.PLAIN, 16));
		lbl_totalUserEntries.setBounds(243, 38, 64, 28);
		contentPane.add(lbl_totalUserEntries);
		
		final JLabel lbl_uploaded = new JLabel("Prints");
		lbl_uploaded.setForeground(new Color(0, 100, 0));
		lbl_uploaded.setFont(new Font("Consolas", Font.PLAIN, 16));
		lbl_uploaded.setBounds(243, 87, 64, 28);
		contentPane.add(lbl_uploaded);
		
		final JLabel lbl_mailSent = new JLabel("Prints");
		lbl_mailSent.setForeground(new Color(0, 100, 0));
		lbl_mailSent.setFont(new Font("Consolas", Font.PLAIN, 16));
		lbl_mailSent.setBounds(243, 137, 64, 28);
		contentPane.add(lbl_mailSent);
		
		final JLabel lbl_prints = new JLabel("Prints");
		lbl_prints.setForeground(new Color(0, 100, 0));
		lbl_prints.setFont(new Font("Consolas", Font.PLAIN, 16));
		lbl_prints.setBounds(243, 189, 64, 28);
		contentPane.add(lbl_prints);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			MainMenu menu=new MainMenu();
			setVisible(false);
			menu.setVisible(true);
			}
		});
		btnBack.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnBack.setBackground(new Color(255, 255, 255));
		btnBack.setBounds(45, 249, 89, 28);
		contentPane.add(btnBack);
		
		JButton btnClearUserData = new JButton("Clear User Data");
		btnClearUserData.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int ans=JOptionPane.showConfirmDialog(null, "Do you want to clear user data?");
				if(ans==0)
				{
				try {
					DBO.UpdateSettings.clearUserData();
					MainMenu menu=new MainMenu();
					setVisible(false);
					menu.setVisible(true);
					
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				}
				
			}
		});
		btnClearUserData.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnClearUserData.setBackground(Color.WHITE);
		btnClearUserData.setBounds(192, 249, 151, 28);
		contentPane.add(btnClearUserData);
		lbl_mailSent.setText(DTO.UserDTO.getTotalMailSent());
		lbl_prints.setText(DTO.UserDTO.getTotalPrinted());
		lbl_totalUserEntries.setText(DTO.UserDTO.getTotalPhotographs());
		lbl_uploaded.setText(UserDTO.getPendingForUpload());
		setLocationRelativeTo ( null );
	}
	
	
	
}
