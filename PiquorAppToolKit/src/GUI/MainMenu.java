package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.print.attribute.standard.MediaSize.Other;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.SQLException;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.UIManager;

public class MainMenu extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainMenu frame = new MainMenu();
					frame.setVisible(true);
					frame.setLocationRelativeTo ( null );
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainMenu() {
		setBackground(new Color(255, 255, 255));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 748, 352);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 255, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(UIManager.getColor("CheckBox.background"));
		panel.setBounds(20, 22, 342, 270);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JButton btnPiquorFeaturesSettings = new JButton("");
		btnPiquorFeaturesSettings.setIcon(new ImageIcon("C:\\Users\\Neeraj\\Desktop\\features.jpg"));
		btnPiquorFeaturesSettings.setBounds(21, 26, 300, 45);
		panel.add(btnPiquorFeaturesSettings);
		btnPiquorFeaturesSettings.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				AppFunctions app;
				try {
					app = new AppFunctions();
					app.setVisible(true);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
		});
		btnPiquorFeaturesSettings.setBackground(new Color(255, 255, 255));
		btnPiquorFeaturesSettings.setFont(new Font("Consolas", Font.PLAIN, 14));
		
		JButton btnCameraSettings = new JButton("");
		btnCameraSettings.setIcon(new ImageIcon("C:\\Users\\Neeraj\\Desktop\\CAMPAIGN cAMERA.jpg"));
		btnCameraSettings.setBounds(21, 106, 300, 45);
		panel.add(btnCameraSettings);
		btnCameraSettings.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				CameraSettings camera;
				try {
					camera = new CameraSettings();
					camera.setVisible(true);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
		});
		btnCameraSettings.setBackground(new Color(255, 255, 255));
		btnCameraSettings.setFont(new Font("Consolas", Font.PLAIN, 14));
		
		JButton btnSmsmmsscreensaverimagemagick = new JButton("");
		btnSmsmmsscreensaverimagemagick.setIcon(new ImageIcon("C:\\Users\\Neeraj\\Desktop\\sms.jpg"));
		btnSmsmmsscreensaverimagemagick.setBounds(21, 194, 300, 45);
		panel.add(btnSmsmmsscreensaverimagemagick);
		btnSmsmmsscreensaverimagemagick.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				Others oth;
				try {
					oth = new Others();
					oth.setVisible(true);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
		});
		btnSmsmmsscreensaverimagemagick.setBackground(new Color(255, 255, 255));
		btnSmsmmsscreensaverimagemagick.setFont(new Font("Consolas", Font.PLAIN, 14));
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(UIManager.getColor("CheckBox.background"));
		panel_1.setBounds(384, 22, 326, 183);
		contentPane.add(panel_1);
		panel_1.setLayout(null);
		
		JButton btnConfigureANew = new JButton("");
		btnConfigureANew.setIcon(new ImageIcon("C:\\Users\\Neeraj\\Desktop\\newFrame.jpg"));
		btnConfigureANew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			setVisible(false);
			FrameConfiguration conf=new FrameConfiguration();
			conf.setVisible(true);
			}
		});
		btnConfigureANew.setFont(new Font("Consolas", Font.PLAIN, 14));
		btnConfigureANew.setBackground(Color.WHITE);
		btnConfigureANew.setBounds(33, 23, 257, 45);
		panel_1.add(btnConfigureANew);
		
		JButton btnEditupdatedeleteFrames = new JButton("");
		btnEditupdatedeleteFrames.setIcon(new ImageIcon("C:\\Users\\Neeraj\\Desktop\\editFrame.jpg"));
		btnEditupdatedeleteFrames.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				FrameList list;
				try {
					list = new FrameList();
					list.setVisible(true);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				
			}
		});
		btnEditupdatedeleteFrames.setFont(new Font("Consolas", Font.PLAIN, 14));
		btnEditupdatedeleteFrames.setBackground(Color.WHITE);
		btnEditupdatedeleteFrames.setBounds(33, 111, 257, 45);
		panel_1.add(btnEditupdatedeleteFrames);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(UIManager.getColor("CheckBox.background"));
		panel_2.setBounds(384, 227, 326, 65);
		contentPane.add(panel_2);
		panel_2.setLayout(null);
		
		JButton btnUserData = new JButton("");
		btnUserData.setIcon(new ImageIcon("C:\\Users\\Neeraj\\Desktop\\userData.jpg"));
		btnUserData.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					UserData a=new UserData();
					setVisible(false);
					a.setVisible(true);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnUserData.setFont(new Font("Consolas", Font.PLAIN, 14));
		btnUserData.setBackground(Color.WHITE);
		btnUserData.setBounds(34, 11, 257, 45);
		panel_2.add(btnUserData);
		setLocationRelativeTo ( null );
	}
}
