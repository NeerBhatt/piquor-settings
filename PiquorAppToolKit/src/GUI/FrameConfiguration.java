package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.SQLException;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.ImageIcon;

public class FrameConfiguration extends JFrame {
	private JPanel contentPane;
	private JTextField txtFrameWidth;
	private JTextField txtPhotoX;
	private JTextField txtPhotoY;
	private JTextField txtScaleX;
	private JTextField txtScaleY;
	private JTextField txtRotate;
	private JTextField txtThumbnail;
	private JTextField txtForeground;
	private JTextField txtBackground;
	private JTextField txtCampaignId;
	private JTextField txtFrameHeight;
	private JTextField txtFrameId;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrameConfiguration frame = new FrameConfiguration();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrameConfiguration() {
		setResizable(false);
		setMinimumSize(new Dimension(700, 400));
		setMaximumSize(new Dimension(700, 400));
		setPreferredSize(new Dimension(700, 400));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 700, 385);
		contentPane = new JPanel();
		contentPane.setMinimumSize(new Dimension(700, 500));
		contentPane.setMaximumSize(new Dimension(700, 500));
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblFrameConfiguration = new JLabel("Add New Frame");
		lblFrameConfiguration.setForeground(new Color(102, 0, 255));
		lblFrameConfiguration.setFont(new Font("Times New Roman", Font.PLAIN, 20));
		lblFrameConfiguration.setBackground(Color.WHITE);
		lblFrameConfiguration.setBounds(251, 11, 147, 27);
		contentPane.add(lblFrameConfiguration);
		
		final JPanel panel = new JPanel();
		panel.setBounds(326, 49, 346, 148);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblThumbnail = new JLabel("Thumbnail");
		lblThumbnail.setBounds(7, 22, 72, 19);
		panel.add(lblThumbnail);
		lblThumbnail.setFont(new Font("Consolas", Font.PLAIN, 15));
		txtThumbnail = new JTextField();
		txtThumbnail.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent arg0) {
			
			}
		});
		txtThumbnail.setBounds(153, 21, 183, 20);
		panel.add(txtThumbnail);
		txtThumbnail.setText((String) null);
		txtThumbnail.setColumns(10);
		
		JLabel lblForegroundFrame = new JLabel("Foreground Frame");
		lblForegroundFrame.setBounds(7, 64, 136, 26);
		panel.add(lblForegroundFrame);
		lblForegroundFrame.setFont(new Font("Consolas", Font.PLAIN, 15));
		
		txtForeground = new JTextField();
		txtForeground.setBounds(153, 67, 183, 20);
		panel.add(txtForeground);
		txtForeground.setText((String) null);
		txtForeground.setColumns(10);
		
		JLabel lblBackgroundFrame = new JLabel("Background Frame");
		lblBackgroundFrame.setBounds(10, 100, 138, 26);
		panel.add(lblBackgroundFrame);
		lblBackgroundFrame.setFont(new Font("Consolas", Font.PLAIN, 15));
		
		txtBackground = new JTextField();
		txtBackground.setBounds(153, 104, 183, 20);
		panel.add(txtBackground);
		txtBackground.setText((String) null);
		txtBackground.setColumns(10);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBounds(503, 214, 167, 87);
		contentPane.add(panel_3);
		panel_3.setLayout(null);
		
		JLabel lblScaleX = new JLabel("Scale X");
		lblScaleX.setBounds(10, 11, 74, 26);
		panel_3.add(lblScaleX);
		lblScaleX.setFont(new Font("Consolas", Font.PLAIN, 15));
		
		txtScaleX = new JTextField();
		txtScaleX.setBounds(83, 14, 74, 20);
		panel_3.add(txtScaleX);
		txtScaleX.setText((String) null);
		txtScaleX.setColumns(10);
		
		JLabel lblScaleY = new JLabel("Scale Y");
		lblScaleY.setBounds(10, 48, 74, 26);
		panel_3.add(lblScaleY);
		lblScaleY.setFont(new Font("Consolas", Font.PLAIN, 15));
		
		txtScaleY = new JTextField();
		txtScaleY.setBounds(83, 51, 74, 20);
		panel_3.add(txtScaleY);
		txtScaleY.setText((String) null);
		txtScaleY.setColumns(10);
		
		JPanel panel_4 = new JPanel();
		panel_4.setBounds(326, 214, 167, 87);
		contentPane.add(panel_4);
		panel_4.setLayout(null);
		
		JLabel lblPhotoX = new JLabel("Photo X ");
		lblPhotoX.setBounds(10, 11, 74, 26);
		panel_4.add(lblPhotoX);
		lblPhotoX.setFont(new Font("Consolas", Font.PLAIN, 15));
		
		txtPhotoX = new JTextField();
		txtPhotoX.setBounds(82, 14, 74, 20);
		panel_4.add(txtPhotoX);
		txtPhotoX.setText((String) null);
		txtPhotoX.setColumns(10);
		
		JLabel lblPhotoY = new JLabel("Photo Y ");
		lblPhotoY.setBounds(10, 44, 89, 26);
		panel_4.add(lblPhotoY);
		lblPhotoY.setFont(new Font("Consolas", Font.PLAIN, 15));
		
		txtPhotoY = new JTextField();
		txtPhotoY.setBounds(82, 47, 74, 20);
		panel_4.add(txtPhotoY);
		txtPhotoY.setText((String) null);
		txtPhotoY.setColumns(10);
		
		JPanel panel_5 = new JPanel();
		panel_5.setBounds(10, 158, 292, 102);
		contentPane.add(panel_5);
		panel_5.setLayout(null);
		
		JLabel lblFrameHeight = new JLabel("Frame Height");
		lblFrameHeight.setBounds(10, 41, 100, 26);
		panel_5.add(lblFrameHeight);
		lblFrameHeight.setFont(new Font("Consolas", Font.PLAIN, 15));
		
		txtFrameWidth = new JTextField();
		txtFrameWidth.setBounds(122, 14, 76, 20);
		panel_5.add(txtFrameWidth);
		txtFrameWidth.setText("1120");
		txtFrameWidth.setColumns(10);
		
		JLabel lblFrameWidth = new JLabel("Frame Width");
		lblFrameWidth.setBounds(10, 11, 100, 26);
		panel_5.add(lblFrameWidth);
		lblFrameWidth.setFont(new Font("Consolas", Font.PLAIN, 15));
		
		txtFrameHeight = new JTextField();
		txtFrameHeight.setBounds(120, 44, 78, 20);
		panel_5.add(txtFrameHeight);
		txtFrameHeight.setText("800");
		txtFrameHeight.setColumns(10);
		
		JLabel lblRotate = new JLabel("Rotate");
		lblRotate.setBounds(10, 72, 48, 19);
		panel_5.add(lblRotate);
		lblRotate.setFont(new Font("Consolas", Font.PLAIN, 15));
		
		txtRotate = new JTextField();
		txtRotate.setBounds(122, 71, 77, 20);
		panel_5.add(txtRotate);
		txtRotate.setText("0");
		txtRotate.setColumns(10);
		
		JPanel panel_6 = new JPanel();
		panel_6.setBounds(10, 49, 292, 87);
		contentPane.add(panel_6);
		panel_6.setLayout(null);
		
		JLabel lblFrameId = new JLabel("Frame Id");
		lblFrameId.setBounds(14, 11, 100, 26);
		panel_6.add(lblFrameId);
		lblFrameId.setFont(new Font("Consolas", Font.PLAIN, 15));
		
		JLabel lblCampaignId = new JLabel("Campaign Id");
		lblCampaignId.setBounds(14, 48, 100, 26);
		panel_6.add(lblCampaignId);
		lblCampaignId.setFont(new Font("Consolas", Font.PLAIN, 15));
		
		txtFrameId = new JTextField();
		txtFrameId.setBounds(124, 14, 158, 20);
		panel_6.add(txtFrameId);
		txtFrameId.setText((String) null);
		txtFrameId.setColumns(10);
		
		txtCampaignId = new JTextField();
		txtCampaignId.setBounds(124, 51, 158, 20);
		panel_6.add(txtCampaignId);
		txtCampaignId.setText((String) null);
		txtCampaignId.setColumns(10);
		
		JButton btnNewButton = new JButton("Back");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				MainMenu menu=new MainMenu();
				menu.setVisible(true);
				
			}
		});
		btnNewButton.setBackground(Color.WHITE);
		btnNewButton.setBounds(10, 296, 99, 32);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Add");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String frameId="", campaignId="", frameHeight="", frameWidth="", photoX="", photoY="", scaleX="", scaleY="", rotate="", thumbnail="", foreground="",background="";
				frameId=txtFrameId.getText().trim();
				campaignId=txtCampaignId.getText().trim();
				frameHeight=txtFrameHeight.getText().trim();
				frameWidth=txtFrameWidth.getText().trim();
				photoX=txtPhotoX.getText().trim();
				photoY=txtPhotoY.getText().trim();
				scaleX=txtScaleX.getText().trim();
				scaleY=txtScaleY.getText().trim();
				rotate=txtRotate.getText().trim();
				thumbnail=txtThumbnail.getText().trim();
				foreground=txtForeground.getText().trim();
				background=txtBackground.getText().trim();
				
				if(frameId.equals("")|| campaignId.equals("")|| frameHeight.equals("")|| frameWidth.equals("")|| photoX.equals("")||photoY.equals("")||scaleX.equals("")||scaleY.equals("")||rotate.equals("")||thumbnail.equals("")||foreground.equals("")||background.equals("") )
				{
					JOptionPane.showMessageDialog(null,"Field(s) are empty");
				}
				else
				{
					try {
						if(!DBO.UpdateSettings.isFrameIdExists(frameId))
							DBO.UpdateSettings.addNewFrame(frameId, campaignId, frameHeight, frameWidth, photoX, photoY, scaleX, scaleY, rotate, thumbnail, foreground, background);
						else
							JOptionPane.showMessageDialog(null,"Frame Id already exists");
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
			}
		});
		btnNewButton_1.setBackground(Color.WHITE);
		btnNewButton_1.setBounds(143, 296, 89, 32);
		contentPane.add(btnNewButton_1);
		setLocationRelativeTo ( null );
	}
}
