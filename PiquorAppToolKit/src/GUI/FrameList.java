package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Dimension;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JList;
import javax.swing.AbstractListModel;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;

import java.awt.Color;
import java.awt.Cursor;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.DefaultComboBoxModel;

import Utility.DBUtil;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

public class FrameList extends JFrame {
	static	String frameId[]=null;
	private JPanel contentPane;
	JComboBox comboBox = new JComboBox();
	private JTextField txtFrameId;
	private JTextField txtCampaignId;
	private JTextField txtThumbnail;
	private JTextField txtForeground;
	private JTextField txtBackground;
	private JTextField txtFrameWidth;
	private JTextField txtFrameHeight;
	private JTextField txtRotate;
	private JTextField txtPhotoX;
	private JTextField txtPhotoY;
	private JTextField txtScaleX;
	private JTextField txtScaleY;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrameList frame = new FrameList();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws SQLException 
	 */
	public FrameList() throws SQLException {
		setMaximumSize(new Dimension(700, 500));
		setMinimumSize(new Dimension(700, 500));
		setPreferredSize(new Dimension(700, 500));
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setForeground(Color.BLACK);
		contentPane.setBackground(Color.WHITE);
		contentPane.setPreferredSize(new Dimension(400, 400));
		contentPane.setMinimumSize(new Dimension(400, 400));
		contentPane.setMaximumSize(new Dimension(400, 400));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		final JPanel panel = new JPanel();
		comboBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				if(!comboBox.getSelectedItem().toString().trim().equals("Select Frame ID"))
				{
					panel.setVisible(true);
					//JOptionPane.showMessageDialog(null, "hit");
					String selectedFrameId=comboBox.getSelectedItem().toString().trim();
					
					
					
					String query="select CampaignId," +
					"FinalFrameHeight," +
					"FinalFrameWidth," +
					"PhotoX," +
					"PhotoY," +
					"ScaleX," +
					"ScaleY," +
					"Rotate," +
					"Thumbnail," +
					"ForegroundFrame," +
					"BackgroundFrame from campaign_image_frame_configuration where FrameId='"+selectedFrameId+"'";
					Connection con=null;
					Statement stmt=null;
					try{
						txtFrameId.setText(selectedFrameId);
						con=DBUtil.getConnection();
						stmt=con.createStatement();
						ResultSet res=null;
						res=stmt.executeQuery(query);
						
						while(res.next())
						{
							txtCampaignId.setText(res.getString(1));
							txtFrameHeight.setText(res.getString(2));
							txtFrameWidth.setText(res.getString(3));
							txtPhotoX.setText(res.getString(4));
							txtPhotoY.setText(res.getString(5));
							txtScaleX.setText(res.getString(6));
							txtScaleY.setText(res.getString(7));
							txtRotate.setText(res.getString(8));
							txtThumbnail.setText(res.getString(9));
							txtForeground.setText(res.getString(10));
							txtBackground.setText(res.getString(11));
							
						}
					//	System.out.println(frames);
					}
						catch(Exception e)
						{
							System.out.println("Error Occured while fetching frames::"+e.getMessage());
							JOptionPane.showMessageDialog(null,"Error Occured :: "+e.getMessage());
						}
						finally
						{
							if(stmt!=null)
							{
								try {
									stmt.close();
								} catch (SQLException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
							
							if(con!=null)
							{
								try {
									con.close();
								} catch (SQLException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						}
					
					
					
					
				}
					else
					{
					panel.setVisible(false);
					JOptionPane.showMessageDialog(null, "Please Select Frame Id");
					}
					// TODO Auto-generated method stub
				
			}
		});
		comboBox.setFont(new Font("Consolas", Font.PLAIN, 14));
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Select Frame ID"}));
		comboBox.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		comboBox.setBackground(Color.WHITE);
		
		
		comboBox.setBounds(186, 50, 186, 34);
		contentPane.add(comboBox);
		
		
		panel.setVisible(false);
		panel.setBounds(0, 95, 694, 344);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.WHITE);
		panel_1.setLayout(null);
		panel_1.setBounds(10, 21, 292, 87);
		panel.add(panel_1);
		
		JLabel label = new JLabel("Frame Id");
		label.setFont(new Font("Consolas", Font.PLAIN, 15));
		label.setBounds(14, 11, 100, 26);
		panel_1.add(label);
		
		JLabel label_1 = new JLabel("Campaign Id");
		label_1.setFont(new Font("Consolas", Font.PLAIN, 15));
		label_1.setBounds(14, 48, 100, 26);
		panel_1.add(label_1);
		
		txtFrameId = new JTextField();
		txtFrameId.setEditable(false);
		txtFrameId.setText((String) null);
		txtFrameId.setColumns(10);
		txtFrameId.setBounds(124, 14, 158, 20);
		panel_1.add(txtFrameId);
		
		txtCampaignId = new JTextField();
		txtCampaignId.setText((String) null);
		txtCampaignId.setColumns(10);
		txtCampaignId.setBounds(124, 51, 158, 20);
		panel_1.add(txtCampaignId);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(Color.WHITE);
		panel_2.setLayout(null);
		panel_2.setBounds(326, 21, 346, 148);
		panel.add(panel_2);
		
		JLabel label_2 = new JLabel("Thumbnail");
		label_2.setFont(new Font("Consolas", Font.PLAIN, 15));
		label_2.setBounds(7, 22, 72, 19);
		panel_2.add(label_2);
		
		txtThumbnail = new JTextField();
		txtThumbnail.setText((String) null);
		txtThumbnail.setColumns(10);
		txtThumbnail.setBounds(153, 21, 183, 20);
		panel_2.add(txtThumbnail);
		
		JLabel label_3 = new JLabel("Foreground Frame");
		label_3.setFont(new Font("Consolas", Font.PLAIN, 15));
		label_3.setBounds(7, 64, 136, 26);
		panel_2.add(label_3);
		
		txtForeground = new JTextField();
		txtForeground.setText((String) null);
		txtForeground.setColumns(10);
		txtForeground.setBounds(153, 67, 183, 20);
		panel_2.add(txtForeground);
		
		JLabel label_4 = new JLabel("Background Frame");
		label_4.setFont(new Font("Consolas", Font.PLAIN, 15));
		label_4.setBounds(10, 100, 138, 26);
		panel_2.add(label_4);
		
		txtBackground = new JTextField();
		txtBackground.setText((String) null);
		txtBackground.setColumns(10);
		txtBackground.setBounds(153, 104, 183, 20);
		panel_2.add(txtBackground);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBackground(Color.WHITE);
		panel_3.setLayout(null);
		panel_3.setBounds(10, 130, 292, 102);
		panel.add(panel_3);
		
		JLabel label_5 = new JLabel("Frame Height");
		label_5.setFont(new Font("Consolas", Font.PLAIN, 15));
		label_5.setBounds(10, 41, 100, 26);
		panel_3.add(label_5);
		
		txtFrameWidth = new JTextField();
		txtFrameWidth.setText("1120");
		txtFrameWidth.setColumns(10);
		txtFrameWidth.setBounds(122, 14, 76, 20);
		panel_3.add(txtFrameWidth);
		
		JLabel label_6 = new JLabel("Frame Width");
		label_6.setFont(new Font("Consolas", Font.PLAIN, 15));
		label_6.setBounds(10, 11, 100, 26);
		panel_3.add(label_6);
		
		txtFrameHeight = new JTextField();
		txtFrameHeight.setText("800");
		txtFrameHeight.setColumns(10);
		txtFrameHeight.setBounds(120, 44, 78, 20);
		panel_3.add(txtFrameHeight);
		
		JLabel label_7 = new JLabel("Rotate");
		label_7.setFont(new Font("Consolas", Font.PLAIN, 15));
		label_7.setBounds(10, 72, 48, 19);
		panel_3.add(label_7);
		
		txtRotate = new JTextField();
		txtRotate.setText("0");
		txtRotate.setColumns(10);
		txtRotate.setBounds(122, 71, 77, 20);
		panel_3.add(txtRotate);
		
		JPanel panel_4 = new JPanel();
		panel_4.setBackground(Color.WHITE);
		panel_4.setLayout(null);
		panel_4.setBounds(326, 186, 167, 87);
		panel.add(panel_4);
		
		JLabel label_8 = new JLabel("Photo X ");
		label_8.setFont(new Font("Consolas", Font.PLAIN, 15));
		label_8.setBounds(10, 11, 74, 26);
		panel_4.add(label_8);
		
		txtPhotoX = new JTextField();
		txtPhotoX.setText((String) null);
		txtPhotoX.setColumns(10);
		txtPhotoX.setBounds(82, 14, 74, 20);
		panel_4.add(txtPhotoX);
		
		JLabel label_9 = new JLabel("Photo Y ");
		label_9.setFont(new Font("Consolas", Font.PLAIN, 15));
		label_9.setBounds(10, 44, 89, 26);
		panel_4.add(label_9);
		
		txtPhotoY = new JTextField();
		txtPhotoY.setText((String) null);
		txtPhotoY.setColumns(10);
		txtPhotoY.setBounds(82, 47, 74, 20);
		panel_4.add(txtPhotoY);
		
		JPanel panel_5 = new JPanel();
		panel_5.setBackground(Color.WHITE);
		panel_5.setLayout(null);
		panel_5.setBounds(503, 186, 167, 87);
		panel.add(panel_5);
		
		JLabel label_10 = new JLabel("Scale X");
		label_10.setFont(new Font("Consolas", Font.PLAIN, 15));
		label_10.setBounds(10, 11, 74, 26);
		panel_5.add(label_10);
		
		txtScaleX = new JTextField();
		txtScaleX.setText((String) null);
		txtScaleX.setColumns(10);
		txtScaleX.setBounds(83, 14, 74, 20);
		panel_5.add(txtScaleX);
		
		JLabel label_11 = new JLabel("Scale Y");
		label_11.setFont(new Font("Consolas", Font.PLAIN, 15));
		label_11.setBounds(10, 48, 74, 26);
		panel_5.add(label_11);
		
		txtScaleY = new JTextField();
		txtScaleY.setText((String) null);
		txtScaleY.setColumns(10);
		txtScaleY.setBounds(83, 51, 74, 20);
		panel_5.add(txtScaleY);
		
		JButton btnUpdate = new JButton("Update");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			String frameId = null, campaignId = null, frameHeight= null, frameWidth= null, photoX= null, photoY= null, scaleX= null, scaleY = null, rotate= null, thumbnail= null, foreground= null, background= null;
			frameId=txtFrameId.getText().trim();
			campaignId=txtCampaignId.getText().trim();
			frameHeight=txtFrameHeight.getText().trim();
			frameWidth=txtFrameWidth.getText().trim();
			photoX=txtPhotoX.getText().trim();
			photoY=txtPhotoY.getText().trim();
			scaleX=txtScaleX.getText().trim();
			scaleY=txtScaleY.getText().trim();
			rotate=txtRotate.getText().trim();
			thumbnail=txtThumbnail.getText().trim();
			foreground=txtForeground.getText().trim();
			background=txtBackground.getText().trim();
			
			if(frameId.equals("")|| campaignId.equals("")|| frameHeight.equals("")|| frameWidth.equals("")|| photoX.equals("")||photoY.equals("")||scaleX.equals("")||scaleY.equals("")||rotate.equals("")||thumbnail.equals("")||foreground.equals("")||background.equals("") )
			{
				JOptionPane.showMessageDialog(null,"Field(s) are empty");
			}
			else
			{	
			
				try {
					DBO.UpdateSettings.updateFrame(frameId, campaignId, frameHeight, frameWidth, photoX, photoY, scaleX, scaleY, rotate, thumbnail, foreground, background);
					
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
				
			};
		});
		btnUpdate.setBackground(Color.WHITE);
		btnUpdate.setBounds(143, 268, 89, 32);
		panel.add(btnUpdate);
		
		final JButton btnDelete = new JButton("Delete");
		final JButton btnDeleteAllFrames = new JButton("Delete All Frames");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int ans=JOptionPane.showConfirmDialog(null, "Do you want to delete frame?");
				if(ans==0)
				{
				try {
					DBO.UpdateSettings.deleteSelectedFrame(txtFrameId.getText().trim());
					panel.setVisible(false);
					//loadComboBox();
					comboBox.removeItem(txtFrameId.getText());
					if(DBO.UpdateSettings.countFrames()<1)
					{
						btnDeleteAllFrames.setVisible(false);
						JOptionPane.showMessageDialog(null,"No frame Configured!");
					}
					
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				}
				
			}
		});
		btnDelete.setBackground(Color.WHITE);
		btnDelete.setBounds(29, 268, 89, 32);
		panel.add(btnDelete);
		
		
		btnDeleteAllFrames.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int ans=JOptionPane.showConfirmDialog(null, "Do you want to delete all frames?");
				if(ans==0)
				{
				try {
					DBO.UpdateSettings.deleteAllFrames();
					panel.setVisible(false);
					btnDelete.setVisible(false);
					
					for(int i=0;i<frameId.length;i++)
						comboBox.removeItem(frameId[i]);
					//loadComboBox();
					btnDeleteAllFrames.setVisible(false);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				}
				
			}
		});
		btnDeleteAllFrames.setBackground(Color.WHITE);
		btnDeleteAllFrames.setBounds(448, 50, 158, 34);
		contentPane.add(btnDeleteAllFrames);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				MainMenu m=new MainMenu();
				m.setVisible(true);
			}
		});
		btnBack.setBackground(Color.WHITE);
		btnBack.setBounds(23, 50, 97, 34);
		contentPane.add(btnBack);
		
		if(DBO.UpdateSettings.countFrames()<1)
		{
			btnDeleteAllFrames.setVisible(false);
			JOptionPane.showMessageDialog(null,"No frame Configured!");
		}
			else
				loadComboBox();
		setLocationRelativeTo ( null );
	}

	public void loadComboBox()
	{
		try {
			
			int lenght=DBO.UpdateSettings.frameList().length;
		 frameId=DBO.UpdateSettings.frameList();
			for(int i=0;i<lenght;i++)
			{
				comboBox.addItem(frameId[i]);
			}
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
