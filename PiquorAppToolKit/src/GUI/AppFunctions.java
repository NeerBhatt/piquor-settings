package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JCheckBox;
import java.awt.Component;
import java.sql.SQLException;

import javax.swing.JButton;

import org.omg.DynamicAny.DynAnyOperations;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AppFunctions extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AppFunctions frame = new AppFunctions();
					frame.setVisible(true);
					frame.setLocationRelativeTo ( null );
				    
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws SQLException 
	 */
	public AppFunctions() throws SQLException {
	DAO.MachineConfigFileDAO.loadMachineConfig();
		setResizable(false);
		setVisible(true);
		setMinimumSize(new Dimension(700, 500));
		setMaximumSize(new Dimension(400, 400));
		setBackground(Color.WHITE);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 255, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Choose Piquor App Features");
		lblNewLabel.setForeground(new Color(0, 51, 102));
		lblNewLabel.setFont(new Font("Consolas", Font.BOLD, 24));
		lblNewLabel.setBounds(165, 11, 349, 35);
		contentPane.add(lblNewLabel);
		
		final JCheckBox chkemail = new JCheckBox("Email");
		chkemail.setFont(new Font("Consolas", Font.PLAIN, 18));
		chkemail.setBackground(new Color(255, 255, 255));
		chkemail.setAlignmentX(Component.CENTER_ALIGNMENT);
		chkemail.setRequestFocusEnabled(false);
		chkemail.setBounds(61, 64, 101, 40);
		contentPane.add(chkemail);
		
		final JCheckBox chkFacebook = new JCheckBox("Facebook");
		chkFacebook.setFont(new Font("Consolas", Font.PLAIN, 18));
		chkFacebook.setBackground(new Color(255, 255, 255));
		chkFacebook.setAlignmentX(Component.CENTER_ALIGNMENT);
		chkFacebook.setBounds(297, 64, 116, 40);
		contentPane.add(chkFacebook);
		
		final JCheckBox chkTwitter = new JCheckBox("Twitter");
		chkTwitter.setFont(new Font("Consolas", Font.PLAIN, 18));
		chkTwitter.setBackground(new Color(255, 255, 255));
		chkTwitter.setAlignmentX(Component.CENTER_ALIGNMENT);
		chkTwitter.setRequestFocusEnabled(false);
		chkTwitter.setBounds(528, 64, 101, 40);
		contentPane.add(chkTwitter);
		
		final JCheckBox chckbxSms = new JCheckBox("SMS");
		chckbxSms.setFont(new Font("Consolas", Font.PLAIN, 18));
		chckbxSms.setBackground(new Color(255, 255, 255));
		chckbxSms.setAlignmentX(Component.CENTER_ALIGNMENT);
		chckbxSms.setRequestFocusEnabled(false);
		chckbxSms.setBounds(528, 127, 70, 40);
		contentPane.add(chckbxSms);
		
		final JCheckBox chckbxMms = new JCheckBox("MMS");
		chckbxMms.setFont(new Font("Consolas", Font.PLAIN, 18));
		chckbxMms.setBackground(new Color(255, 255, 255));
		chckbxMms.setAlignmentX(Component.CENTER_ALIGNMENT);
		chckbxMms.setRequestFocusEnabled(false);
		chckbxMms.setBounds(297, 127, 77, 40);
		contentPane.add(chckbxMms);
		
		final JCheckBox chckbxPrint = new JCheckBox("Print");
		chckbxPrint.setFont(new Font("Consolas", Font.PLAIN, 18));
		chckbxPrint.setBackground(new Color(255, 255, 255));
		chckbxPrint.setAlignmentX(Component.CENTER_ALIGNMENT);
		chckbxPrint.setRequestFocusEnabled(false);
		chckbxPrint.setBounds(61, 127, 101, 40);
		contentPane.add(chckbxPrint);
		
		final JCheckBox chckbxDobScreen = new JCheckBox("DOB Screen");
		chckbxDobScreen.setFont(new Font("Consolas", Font.PLAIN, 18));
		chckbxDobScreen.setBackground(new Color(255, 255, 255));
		chckbxDobScreen.setAlignmentX(Component.CENTER_ALIGNMENT);
		chckbxDobScreen.setRequestFocusEnabled(false);
		chckbxDobScreen.setBounds(61, 272, 133, 40);
		contentPane.add(chckbxDobScreen);
		
		final JCheckBox chckbxAskForPhone = new JCheckBox("Ask For Phone");
		chckbxAskForPhone.setFont(new Font("Consolas", Font.PLAIN, 18));
		chckbxAskForPhone.setBackground(new Color(255, 255, 255));
		chckbxAskForPhone.setAlignmentX(Component.CENTER_ALIGNMENT);
		chckbxAskForPhone.setRequestFocusEnabled(false);
		chckbxAskForPhone.setBounds(297, 201, 162, 40);
		contentPane.add(chckbxAskForPhone);
		
		final JCheckBox chckbxAskForName = new JCheckBox("Ask For Name");
		chckbxAskForName.setBackground(new Color(255, 255, 255));
		chckbxAskForName.setFont(new Font("Consolas", Font.PLAIN, 18));
		chckbxAskForName.setMaximumSize(new Dimension(107, 23));
		chckbxAskForName.setPreferredSize(new Dimension(107, 23));
		chckbxAskForName.setRequestFocusEnabled(false);
		chckbxAskForName.setBounds(61, 201, 150, 40);
		contentPane.add(chckbxAskForName);
		
		final JCheckBox chckbxFeedbackScreen = new JCheckBox("Feedback Screen");
		chckbxFeedbackScreen.setBackground(new Color(255, 255, 255));
		chckbxFeedbackScreen.setForeground(new Color(0, 0, 0));
		chckbxFeedbackScreen.setFont(new Font("Consolas", Font.PLAIN, 18));
		chckbxFeedbackScreen.setRequestFocusEnabled(false);
		chckbxFeedbackScreen.setBounds(297, 272, 196, 40);
		contentPane.add(chckbxFeedbackScreen);
		
		
		final JCheckBox chckbxRecordvideo = new JCheckBox("Record Video");
		chckbxRecordvideo.setRequestFocusEnabled(false);
		chckbxRecordvideo.setFont(new Font("Consolas", Font.PLAIN, 18));
		chckbxRecordvideo.setBackground(Color.WHITE);
		chckbxRecordvideo.setAlignmentX(0.5f);
		chckbxRecordvideo.setBounds(61, 336, 179, 40);
		contentPane.add(chckbxRecordvideo);
		
		
		JButton btnUpdate = new JButton("Update");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String email="",facebook="",twitter="",print="",sms="",mms="",askForName="",askForPhone="",feedback="",dob="",recordVideo="";
				if(chkemail.isSelected()==true)
					email="1";
				else
					email="0";
				
				if(chkFacebook.isSelected()==true)
					facebook="1";
				else
					facebook="0";
				
				if(chkTwitter.isSelected()==true)
					twitter="1";
				else
					twitter="0";
				
				if(chckbxAskForName.isSelected()==true)
					askForName="1";
				else
					askForName="0";
				
				if(chckbxAskForPhone.isSelected()==true)
					askForPhone="1";
				else
					askForPhone="0";
				
				if(chckbxDobScreen.isSelected()==true)
					dob="1";
				else
					dob="0";
				
				if(chckbxFeedbackScreen.isSelected()==true)
					feedback="1";
				else
					feedback="0";
				
				if(chckbxMms.isSelected()==true)
					mms="1";
				else
					mms="0";
				
				if(chckbxPrint.isSelected()==true)
					print="1";
				else
					print="0";
				
				if(chckbxSms.isSelected()==true)
					sms="1";
				else
					sms="0";
				
				if(chckbxRecordvideo.isSelected()==true)
					recordVideo="1";
				else
					recordVideo="0";
				try{
					
					DBO.UpdateSettings.updatePiquorAppFeatures(email, facebook, twitter, print, sms, mms, askForName, askForPhone, feedback, dob,recordVideo);
				}
				catch(Exception e)
				{
					
				}
				
				
				
			}
		});
		btnUpdate.setBackground(new Color(255, 255, 255));
		btnUpdate.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnUpdate.setBounds(297, 402, 89, 23);
		contentPane.add(btnUpdate);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				MainMenu menu=new MainMenu();
				menu.setVisible(true);
				
			}
		});
		btnBack.setBackground(new Color(255, 255, 255));
		btnBack.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnBack.setBounds(61, 402, 89, 23);
		contentPane.add(btnBack);
		
		
		setLocationRelativeTo ( null );
		
		if(DTO.MachineIdDTO.getEmailButton().equals("1"))
		{
			chkemail.setSelected(true);
		}
		if(DTO.MachineIdDTO.getFacebookButton().equals("1"))
		{
			chkFacebook.setSelected(true);
		}
		if(DTO.MachineIdDTO.getTwitterButton().equals("1"))
		{
			chkTwitter.setSelected(true);
		}
		if(DTO.MachineIdDTO.getPrintButton().equals("1"))
		{
			chckbxPrint.setSelected(true);;
		}
		if(DTO.MachineIdDTO.getSmsButton().equals("1"))
		{
			chckbxSms.setSelected(true);
		}
		if(DTO.MachineIdDTO.getMmsButton().equals("1"))
		{
			chckbxMms.setSelected(true);
		}
		if(DTO.MachineIdDTO.getDobScreen().equals("1"))
		{
			chckbxDobScreen.setSelected(true);;
		}
		if(DTO.MachineIdDTO.getAskForName().equals("1"))
		{
			chckbxAskForName.setSelected(true);
		}
		if(DTO.MachineIdDTO.getAskForPhone().equals("1"))
		{
			chckbxAskForPhone.setSelected(true);
		}
		if(DTO.MachineIdDTO.getFeedbackScreen().equals("1"))
		{
			chckbxFeedbackScreen.setSelected(true);
		}
		if(DTO.MachineIdDTO.getCaptureVideo().equals("1"))
		{
			chckbxRecordvideo.setSelected(true);
		}
		
	
	}
}
