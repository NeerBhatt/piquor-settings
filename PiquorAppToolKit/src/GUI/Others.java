package GUI;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.sql.SQLException;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

public class Others extends JFrame {
	
	
	String imagepath="";
	private JPanel contentPane;
	private JTextField txtSms;
	private JTextField txtMms;
	private JTextField txtScreenSaverTime;
	private final ButtonGroup buttonGroup = new ButtonGroup();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					 
					Others frame = new Others();
					frame.setVisible(true);
					frame.setLocationRelativeTo ( null );
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws SQLException 
	 */
	public Others() throws SQLException {
		
		
		DAO.MachineConfigFileDAO.loadMachineConfig();
		
		setPreferredSize(new Dimension(700, 500));
		setMaximumSize(new Dimension(700, 500));
		setMinimumSize(new Dimension(700, 500));
		setResizable(false);
		setBackground(new Color(255, 255, 255));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 255, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(255, 255, 255));
		panel.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(22, 45, 283, 130);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("SMS Phone Prefix");
		lblNewLabel.setFont(new Font("Consolas", Font.PLAIN, 14));
		lblNewLabel.setBackground(new Color(255, 255, 255));
		lblNewLabel.setBounds(24, 52, 131, 21);
		panel.add(lblNewLabel);
		
		JLabel lblSmsmms = new JLabel("SMS/MMS");
		lblSmsmms.setBounds(92, 11, 63, 21);
		lblSmsmms.setForeground(new Color(0, 51, 102));
		lblSmsmms.setFont(new Font("Consolas", Font.BOLD, 17));
		panel.add(lblSmsmms);
		
		JLabel lblMmsPhonePrefix = new JLabel("MMS Phone Prefix");
		lblMmsPhonePrefix.setFont(new Font("Consolas", Font.PLAIN, 14));
		lblMmsPhonePrefix.setBackground(Color.WHITE);
		lblMmsPhonePrefix.setBounds(24, 98, 131, 21);
		panel.add(lblMmsPhonePrefix);
		
		txtSms = new JTextField();
		txtSms.setBounds(165, 52, 86, 20);
		panel.add(txtSms);
		txtSms.setColumns(10);
		
		txtMms = new JTextField();
		txtMms.setColumns(10);
		txtMms.setBounds(165, 98, 86, 20);
		panel.add(txtMms);
		
		JPanel panel_1 = new JPanel();
		panel_1.setFont(new Font("Tahoma", Font.PLAIN, 11));
		panel_1.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.setBackground(Color.WHITE);
		panel_1.setBounds(60, 226, 569, 138);
		contentPane.add(panel_1);
		panel_1.setLayout(null);
		
		final JLabel lblpath = new JLabel("Screensaver Time");
		lblpath.setBounds(23, 43, 524, 17);
		lblpath.setFont(new Font("Consolas", Font.PLAIN, 10));
		lblpath.setBackground(Color.WHITE);
		panel_1.add(lblpath);
		
		JLabel lblImageMagickPath_1 = new JLabel("Image Magick Path");
		lblImageMagickPath_1.setBounds(206, 11, 153, 21);
		lblImageMagickPath_1.setForeground(new Color(0, 51, 102));
		lblImageMagickPath_1.setFont(new Font("Consolas", Font.BOLD, 17));
		panel_1.add(lblImageMagickPath_1);
		/*
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setVisible(false);
		fileChooser.setForeground(new Color(0, 0, 0));
		fileChooser.setDialogTitle("Brouse Image Magick Directory");
		fileChooser.setBackground(new Color(255, 255, 255));
		fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		fileChooser.setBounds(10, 79, 229, 133);
		panel_1.add(fileChooser);*/
		
		JButton btnSelectFile = new JButton("Browse Image Magick Path");
		btnSelectFile.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnSelectFile.setBackground(new Color(255, 255, 255));
		btnSelectFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				 JFileChooser fileChooser = new JFileChooser();
				 fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			        int returnValue = fileChooser.showOpenDialog(null);
			        if (returnValue == JFileChooser.APPROVE_OPTION) {
			          File selectedFile = fileChooser.getSelectedFile();
			       imagepath=selectedFile.getAbsolutePath().replace("\\", "/")+"/convert";
			    
			       lblpath.setText(imagepath);
			       
			        }
				
			}
		});
		btnSelectFile.setBounds(183, 82, 222, 23);
		panel_1.add(btnSelectFile);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_2.setBackground(Color.WHITE);
		panel_2.setBounds(379, 45, 283, 130);
		contentPane.add(panel_2);
		panel_2.setLayout(null);
		
		JLabel lblScreensaverType = new JLabel("Screensaver Type");
		lblScreensaverType.setBounds(10, 43, 128, 17);
		lblScreensaverType.setFont(new Font("Consolas", Font.PLAIN, 14));
		lblScreensaverType.setBackground(Color.WHITE);
		panel_2.add(lblScreensaverType);
		
		JLabel lblImageMagickPath = new JLabel("Screensaver");
		lblImageMagickPath.setBounds(93, 11, 99, 21);
		lblImageMagickPath.setForeground(new Color(0, 51, 102));
		lblImageMagickPath.setFont(new Font("Consolas", Font.BOLD, 17));
		panel_2.add(lblImageMagickPath);
		
		JLabel lblScreensaverTime = new JLabel("Screensaver Time");
		lblScreensaverTime.setFont(new Font("Consolas", Font.PLAIN, 14));
		lblScreensaverTime.setBackground(Color.WHITE);
		lblScreensaverTime.setBounds(10, 85, 128, 17);
		panel_2.add(lblScreensaverTime);
		
		txtScreenSaverTime = new JTextField();
		txtScreenSaverTime.setColumns(10);
		txtScreenSaverTime.setBounds(148, 83, 86, 20);
		panel_2.add(txtScreenSaverTime);
		
		final JRadioButton rdbtnImage = new JRadioButton("Image");
		buttonGroup.add(rdbtnImage);
		rdbtnImage.setBackground(new Color(255, 255, 255));
		rdbtnImage.setBounds(144, 40, 61, 23);
		panel_2.add(rdbtnImage);
		
		final JRadioButton rdbtnVideo = new JRadioButton("Video");
		buttonGroup.add(rdbtnVideo);
		rdbtnVideo.setBackground(new Color(255, 255, 255));
		rdbtnVideo.setBounds(216, 40, 61, 23);
		panel_2.add(rdbtnVideo);
		
		JButton btnUpdate = new JButton("Update");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
		
				String smsPhonePrefix="",mmsPhonePrefix="",screensaverType="",ScreenSaverTime="",imageMagickPath="";
				smsPhonePrefix=txtSms.getText();
				mmsPhonePrefix=txtMms.getText();
				ScreenSaverTime=txtScreenSaverTime.getText();
				imageMagickPath=lblpath.getText();
				
				if(rdbtnImage.isSelected()==true)
					screensaverType="image";
				if(rdbtnVideo.isSelected()==true)
					screensaverType="video";
				
				try {
					if(!smsPhonePrefix.equals("") && !mmsPhonePrefix.equals("") && !screensaverType.equals("") && !ScreenSaverTime.equals("") && !imageMagickPath.equals("") )
						DBO.UpdateSettings.updateOtherSettings(smsPhonePrefix, mmsPhonePrefix, screensaverType, ScreenSaverTime, imageMagickPath);
					
					else
						JOptionPane.showMessageDialog(null, "Field(s) Empty!");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				
			}
		});
		btnUpdate.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnUpdate.setBackground(new Color(255, 255, 255));
		btnUpdate.setBounds(364, 412, 89, 23);
		contentPane.add(btnUpdate);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				MainMenu menu=new MainMenu();
				menu.setVisible(true);
			}
		});
		btnBack.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnBack.setBackground(new Color(255, 255, 255));
		btnBack.setBounds(243, 412, 89, 23);
		contentPane.add(btnBack);
		setLocationRelativeTo ( null );
		if(DTO.MachineIdDTO.getScreensaverType().equals("image"))
			rdbtnImage.setSelected(true);
		else
			rdbtnVideo.setSelected(true);
		
		txtMms.setText(DTO.MachineIdDTO.getMmsPhonePrefix());
		txtSms.setText(DTO.MachineIdDTO.getSmsPhonePrefix());
		txtScreenSaverTime.setText(DTO.MachineIdDTO.getScreensaverTime());
		lblpath.setText(DTO.MachineIdDTO.getImageMagickPath());
			
		
	}
	private static class __Tmp {
		private static void __tmp() {
			  javax.swing.JPanel __wbp_panel = new javax.swing.JPanel();
		}
	}
}
