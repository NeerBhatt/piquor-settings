package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Dimension;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import java.sql.SQLException;

import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CameraSettings extends JFrame {

	private JPanel contentPane;
	private JTextField txtphotocounter;
	private JTextField txtResX;
	private JTextField TxtResY;
	private JTextField txtCameraID;
	private JTextField txtMachineId;
	private JTextField txtCampaignID;
	private JTextField txtCampaignName;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CameraSettings frame = new CameraSettings();
					frame.setVisible(true);
					frame.setLocationRelativeTo ( null );
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws SQLException 
	 */
	public CameraSettings() throws SQLException {
		
	DAO.MachineConfigFileDAO.loadMachineConfig();
	DAO.MachineConfigFileDAO.loadMachineMapping();
		setBackground(new Color(255, 255, 255));
		setPreferredSize(new Dimension(700, 500));
		setMaximumSize(new Dimension(700, 500));
		setMinimumSize(new Dimension(700, 500));
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 255, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblCameraSettings = new JLabel("Campaign / Camera Settings");
		lblCameraSettings.setForeground(new Color(0, 51, 102));
		lblCameraSettings.setFont(new Font("Consolas", Font.BOLD, 24));
		lblCameraSettings.setBounds(166, 11, 463, 35);
		contentPane.add(lblCameraSettings);
		
		JLabel lblCameraId = new JLabel("Camera Id");
		lblCameraId.setFont(new Font("Consolas", Font.PLAIN, 15));
		lblCameraId.setBounds(107, 190, 137, 26);
		contentPane.add(lblCameraId);
		
		JLabel lblPhotoCounterTiming = new JLabel("Photo Counter Timing");
		lblPhotoCounterTiming.setFont(new Font("Consolas", Font.PLAIN, 15));
		lblPhotoCounterTiming.setBounds(107, 245, 174, 26);
		contentPane.add(lblPhotoCounterTiming);
		
		txtphotocounter = new JTextField();
		txtphotocounter.setColumns(10);
		txtphotocounter.setBounds(396, 248, 54, 20);
		contentPane.add(txtphotocounter);
		
		JLabel lblCameraResolutionX = new JLabel("Camera Resolution X");
		lblCameraResolutionX.setFont(new Font("Consolas", Font.PLAIN, 15));
		lblCameraResolutionX.setBounds(107, 301, 174, 26);
		contentPane.add(lblCameraResolutionX);
		
		txtResX = new JTextField();
		txtResX.setColumns(10);
		txtResX.setBounds(396, 304, 54, 20);
		contentPane.add(txtResX);
		
		JLabel lblCameraResolutionY = new JLabel("Camera Resolution Y");
		lblCameraResolutionY.setFont(new Font("Consolas", Font.PLAIN, 15));
		lblCameraResolutionY.setBounds(107, 360, 174, 26);
		contentPane.add(lblCameraResolutionY);
		
		TxtResY = new JTextField();
		TxtResY.setColumns(10);
		TxtResY.setBounds(396, 363, 54, 20);
		contentPane.add(TxtResY);
		
		final JCheckBox chckbxSeeCameraName = new JCheckBox("See Camera Name");
		chckbxSeeCameraName.setFont(new Font("Consolas", Font.PLAIN, 15));
		chckbxSeeCameraName.setBackground(new Color(255, 255, 255));
		chckbxSeeCameraName.setBounds(107, 141, 148, 35);
		contentPane.add(chckbxSeeCameraName);
		
		txtCameraID = new JTextField();
		txtCameraID.setColumns(10);
		txtCameraID.setBounds(396, 193, 54, 20);
		contentPane.add(txtCameraID);
		
		JLabel lblMachineId = new JLabel("Machine Id");
		lblMachineId.setFont(new Font("Consolas", Font.PLAIN, 15));
		lblMachineId.setBounds(343, 54, 95, 26);
		contentPane.add(lblMachineId);
		
		txtMachineId = new JTextField();
		txtMachineId.setText((String) null);
		txtMachineId.setColumns(10);
		txtMachineId.setBounds(548, 57, 106, 20);
		contentPane.add(txtMachineId);
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				MainMenu menu=new MainMenu();
				menu.setVisible(true);
			}
		});
		btnBack.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnBack.setBackground(new Color(255, 255, 255));
		btnBack.setForeground(new Color(0, 0, 0));
		btnBack.setBounds(107, 417, 89, 26);
		contentPane.add(btnBack);
		
		JButton btnUpdate = new JButton("Update");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String seeCameraName="",CameraId="",PhotoCounterTiming="",CameraResX="",CameraResY="",machineId="",campaignName="",campaignId="";
				if(chckbxSeeCameraName.isSelected()==true)
					seeCameraName="1";
				else
					seeCameraName="0";
				CameraId=txtCameraID.getText();
				PhotoCounterTiming=txtphotocounter.getText();
				CameraResX=txtResX.getText();
				CameraResY=TxtResY.getText();
				machineId=txtMachineId.getText();
				campaignId=txtCampaignID.getText();
				campaignName=txtCampaignName.getText();
				try {
					if(txtMachineId.getText().length()>0 && txtCameraID.getText().length()>0 && txtphotocounter.getText().length()>0 && txtResX.getText().length()>0 && TxtResY.getText().length()>0 && txtCampaignID.getText().length()>0 && txtCampaignName.getText().length()>0)
					{
						DBO.UpdateSettings.updateMachineCore(machineId);
						DBO.UpdateSettings.updateMachineMapping(machineId, campaignId, campaignName);
					DBO.UpdateSettings.updateCameraSettings(machineId,seeCameraName, CameraId, PhotoCounterTiming, CameraResX, CameraResY);
					
					}
					else
						JOptionPane.showMessageDialog(null, "Field(s) are empty!");
					} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
			}
		});
		
		
		JLabel lblCampaignId = new JLabel("Campaign Id");
		lblCampaignId.setFont(new Font("Consolas", Font.PLAIN, 15));
		lblCampaignId.setBounds(343, 101, 107, 26);
		contentPane.add(lblCampaignId);
		
		txtCampaignID = new JTextField();
		txtCampaignID.setText((String) null);
		txtCampaignID.setColumns(10);
		txtCampaignID.setBounds(480, 104, 174, 20);
		contentPane.add(txtCampaignID);
		
		JLabel lblCampaignName = new JLabel("Campaign Name");
		lblCampaignName.setFont(new Font("Consolas", Font.PLAIN, 15));
		lblCampaignName.setBounds(31, 101, 107, 26);
		contentPane.add(lblCampaignName);
		
		txtCampaignName = new JTextField();
		txtCampaignName.setText((String) null);
		txtCampaignName.setColumns(10);
		txtCampaignName.setBounds(163, 104, 148, 20);
		contentPane.add(txtCampaignName);
		
		setLocationRelativeTo ( null );
		
		
		btnUpdate.setForeground(Color.BLACK);
		btnUpdate.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnUpdate.setBackground(Color.WHITE);
		btnUpdate.setBounds(396, 417, 89, 26);
		contentPane.add(btnUpdate);
		
		if(DTO.MachineIdDTO.getSeeCameraName().equals("1"))
				chckbxSeeCameraName.setSelected(true);
		
		txtCameraID.setText(DTO.MachineIdDTO.getCameraId());
		txtphotocounter.setText(DTO.MachineIdDTO.getPhotoCounterTiming());
		txtResX.setText(DTO.MachineIdDTO.getCameraResX());
		TxtResY.setText(DTO.MachineIdDTO.getCameraResY());
		txtMachineId.setText(DTO.MachineIdDTO.getMachineId());
		txtCampaignID.setText(DTO.MachineIdDTO.getMappingCampaignId());
		txtCampaignName.setText(DTO.MachineIdDTO.getMappingCampaignName());
		
		
	}
}
